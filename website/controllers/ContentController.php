<?php

class ContentController extends Website_Controller_Action
{

    public function homepageAction()
    {

    }


    public function textAction()
    {
        if ($this->getParam('submit') == 'contact') {
            $this->handleContactFormSubmit();
            $this->view->submitted = true;
        }

        if ($this->getParam('submit') == 'register') {
            $this->handleRegisterFormSubmit();
            $this->view->submitted = true;
        }
    }


    protected function handleContactFormSubmit()
    {
        $mail = new \Pimcore\Mail();
        $mail->setDocument('/mail/contact');
        $mail->setParams([
            'firstName' => $this->getParam('first-name'),
            'lastName' => $this->getParam('last-name'),
            'email' => $this->getParam('email'),
            'age' => $this->getParam('age'),
            'message' => $this->getParam('message'),
            'speciality' => $this->getParam('speciality'),
        ]);
        $mail->send();
    }


    protected function handleRegisterFormSubmit()
    {
        if ($this->getParam('type') == 'family') {
            $this->forward('register-family', 'user', 'PRO');
        }

        if ($this->getParam('type') == 'professional') {
            $this->forward('register', 'user', 'PRO');
        }
    }
}
