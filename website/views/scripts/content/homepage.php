<?php $this->layout()->setLayout('standard'); ?>

<section role="main">
    <div class="wrapper">
        <aside class="main-banner">
            <?php if ($this->editmode): ?>
                <?= $this->image("banner", array(
                    "title" => "Drag banner image here",
                    "width" => 1200,
                    "height" => 400,
                    "thumbnail" => 'home-banner'
                )); ?>
            <?php else: ?>
                <?= $this->image('banner')->getThumbnail('home-banner')->getHTML(); ?>
            <?php endif; ?>
        </aside>

        <div class="leading">
            <?= $this->wysiwyg('leading', ['width' => 1200, 'height' => 120]); ?>
        </div>

        <div class="intro">
            <?= $this->wysiwyg('intro', ['width' => 1200, 'height' => 120]); ?>
        </div>
    </div>
</section>

<section class="features container">
    <div class="wrapper">
        <div class="wrapper-a">
            <div class="wrapper-b">
                <?php $block = $this->block("items", array("manual" => true))->start(); ?>
                <?php while ($block->loop()): ?>
                    <div class="item <?= $block->getCurrent() % 2 == 0 ? 'even' : 'odd'; ?>">
                        <?php $block->blockConstruct(); ?>
                        <?php $block->blockStart(); ?>
                        <?php if ($this->editmode): ?>
                            <?= $this->image("icon", array(
                                "title" => "Drag feature image here",
                                "width" => 155,
                                "height" => 127,
                                "thumbnail" => 'home-feature-icon'
                            )); ?>
                        <?php else: ?>
                            <?= $this->image('icon')->getThumbnail('home-feature-icon')->getHTML(); ?>
                        <?php endif; ?>
                        <div class="feature-content">
                            <span>
                                <?php if ($this->editmode): ?>
                                    <?= $this->textarea('content', ['width' => 144, 'height' => 100]); ?>
                                <?php else: ?>
                                    <?= nl2br($this->textarea('content')); ?>
                                <?php endif; ?>
                            </span>
                        </div>
                        <?php $block->blockEnd(); ?>
                        <?php $block->blockDestruct(); ?>
                    </div>
                <?php endwhile; ?>
                <?php $block->end(); ?>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="wrapper">
        <h2>LEARN MORE:</h2>

        <nav class="in-page-menu">
            <ul>
                <li><a href="/for-families"><span>PARENTS & CARGIVERS</span></a></li>
                <li><a href="/for-professionals"><span>MEDICAL /<br/> FEEDING PROFESSIONALS</span></a></li>
            </ul>
        </nav>
    </div>
</section>