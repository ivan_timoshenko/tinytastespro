<?php $this->layout()->setLayout('standard'); ?>

<section class="header-decoration"></section>

<section>
    <div class="wrapper">
        <div class="leading">
            <?= $this->wysiwyg('leading', ['width' => 705, 'height' => 120]); ?>
        </div>

        <div class="intro">
            <?= $this->wysiwyg('intro', ['width' => 1200, 'height' => 120]); ?>
        </div>
    </div>
</section>

<section role="main">
    <div class="wrapper">
        <?php if ($this->editmode): ?>
            <h2>Following blocks will loop blue/white</h2>
        <?php endif; ?>
        <div class="items chess">
            <?php $areaBlock = $this->areablock("content-items", array("manual" => true))->start(); ?>
            <?php while ($areaBlock->loop()): ?>
                <div class="item <?= $areaBlock->getCurrent() % 2 == 0 ? 'even' : 'odd'; ?>">
                    <?php $areaBlock->blockConstruct(); ?>
                    <?php $areaBlock->blockStart(); ?>
                    <?php $areaBlock->content(); ?>
                    <?php $areaBlock->blockEnd(); ?>
                    <?php $areaBlock->blockDestruct(); ?>
                </div>
            <?php endwhile; ?>
            <?php $areaBlock->end(); ?>
        </div>
    </div>
</section>

<?php if ($this->editmode): ?>
    <h2>Special blocks</h2>
<?php endif; ?>
<?= $this->areablock("special-blocks"); ?>