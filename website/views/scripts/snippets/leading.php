<?php if ($this->editmode) $this->layout()->setLayout('empty'); ?>

<div class="leading">
    <aside class="icon">
        <?= $this->image("icon", array(
            "title" => "Drag banner image here",
            "width" => 127,
            "height" => 107,
            "thumbnail" => array(
                "width" => 127,
                "height" => 107,
                "interlace" => true,
                "quality" => 90
            )
        )); ?>
    </aside>

    <section>
        <?= $this->wysiwyg('text', [
            'width' => 550,
            'height' => 60,
        ]); ?>
    </section>
</div>