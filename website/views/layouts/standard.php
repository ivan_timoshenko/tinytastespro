<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?= $this->document->getTitle(); ?></title>
    <meta name="description" content="<?= $this->document->getDescription(); ?>">
    <meta name="keywords" content="<?= $this->document->getKeywords(); ?>">
    <?= $this->headMeta(); ?>

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <link href="/stylesheets/screen.css" media="screen, projection" rel="stylesheet" type="text/css"/>

    <script src="//use.typekit.net/zfm3gtf.js"></script>
    <script>try {
            Typekit.load();
        } catch (e) {
        }</script>

    <script src="//code.jquery.com/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/js/additional-methods.min.js" type="text/javascript"></script>
    <script src="/js/jquery.mask.min.js" type="text/javascript"></script>
    <script src="/js/jquery.slides.min.js" type="text/javascript"></script>
</head>
<body class="<?= Zend_Controller_Front::getInstance()->getRequest()->getActionName(); ?>">
<header>
    <div class="wrapper">
        <a class="logo" href="/">
            <img src="/images/header-logo.png" alt=""/>
        </a>

        <nav class="login">
            <a href="/account/edit">
                My Account
            </a>
        </nav>

        <?php if ($this->document->getProperty('navButton')): ?>
            <nav class="main-menu" style="margin-top: 15px;">
                <a href="<?= $this->document->getProperty('navigationRoot')->getFullPath(); ?>">
                    <?= $this->document->getProperty('navButton')->getThumbnail()->getHTML(); ?>
                </a>
            </nav>
        <?php endif; ?>

        <nav class="main-menu">
            <?php
            // get root node if there is no document defined (for pages which are routed directly through static route)
            if (!$this->document instanceof Document_Page) {
                $this->document = Document::getById(1);
            }

            // get the document which should be used to start in navigation | default home
            $navStartNode = $this->document->getProperty("navigationRoot");
            if (!$navStartNode instanceof Document_Page) {
                $navStartNode = Document::getById(1);
            }

            //this is used as id prefix for the html menu element
            $htmlIdPrefix = "mainNav_";

            $navigation = $this->pimcoreNavigation()->getNavigation($this->document, $navStartNode, $htmlIdPrefix);
            $this->navigation()->menu()->setUseTranslator(false); // to deactivate the translator provided by the view helper
            $this->navigation($navigation);
            ?>
            <?= $this->navigation()->menu()->renderMenu($navigation, array("maxDepth" => 0)); ?>
        </nav>
    </div>
</header>

<?= $this->layout()->content; ?>

<footer>
    <div class="wrapper">
        <a class="logo" href="/"><img src="/images/footer-logo.png" alt=""/></a>

        <nav class="main-menu">
            <ul>
                <li><a href="/about">About</a></li>
                <li><a href="/contact">Contact</a></li>
            </ul>
        </nav>

        <nav class="share">
            <ul>
                <li><a href="#"><img src="/images/share-fb.png" alt=""/></a></li>
                <li><a href="#"><img src="/images/share-twitter.png" alt=""/></a></li>
            </ul>
        </nav>

        <a class="logo turtle" href=""><img src="/images/footer-logo-turtle.png" alt=""/></a>
    </div>
</footer>

<?= $this->placeholder('scripts'); ?>
</body>
</html>