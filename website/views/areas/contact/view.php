<section class="body-section">
    <div class="wrapper">
        <h2>QUESTIONS?</h2>

        <div style="float: left; width: 50%; text-align: right;">
            <p>
                Feel free to email us <br/>
                for more information:
            </p>
        </div>

        <div style="float: left; width: 45%; text-align: left; padding: 12px 0 0 30px;">
            <p>
                <a class="button mail gray" href="mailto:info@tinytastesworld.com">info@</a>
            </p>
        </div>
    </div>
</section>