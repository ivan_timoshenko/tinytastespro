<section class="container instructions body-section">
    <div class="wrapper">
        <div class="wrapper-a">
            <div class="wrapper-b">
                <?php while ($this->block("step")->loop()): ?>
                    <?php if ($this->editmode): ?>
                        <p>Point to link:</p>
                        <?= $this->input('href'); ?>
                        <p>New page?</p>
                        <?= $this->checkbox('targetBlank'); ?>
                        <p>&nbsp;</p>
                    <?php endif; ?>

                    <?php if (!$this->editmode && $this->input('href')->text): ?>
                        <a class="item" href="<?= $this->input('href')->text; ?>" <?= $this->checkbox('targetBlank')->isChecked() ? 'target="_blank"' : ''; ?>>
                    <?php else: ?>
                        <div class="item">
                    <?php endif; ?>
                        <?php if ($this->editmode): ?>
                            <?= $this->image('illustration', ['width' => 275, 'height' => 203]); ?>
                        <?php else: ?>
                            <?= $this->image('illustration')->getThumbnail(['width' => 275, 'height' => 203])->getHTML(); ?>
                        <?php endif; ?>

                        <span class="step">Step <?= $this->block("step")->getCurrent() + 1; ?></span>

                        <div class="content">
                            <?= $this->wysiwyg(['width' => 275, 'height' => 100]); ?>
                        </div>
                    <?php if (!$this->editmode && $this->input('href')->text): ?>
                        </a>
                    <?php else: ?>
                        </div>
                    <?php endif; ?>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
</section>