<section class="container pricing">
    <div class="wrapper">
        <div class="wysiwyg">
            <?= $this->wysiwyg('intro', ['width' => 1200, 'height' => 50]); ?>
        </div>
        <div class="wrapper-a">
            <div class="wrapper-b">
                <?php while ($this->block("step")->loop()): ?>
                    <div class="item">
                        <div class="content">
                            <?= $this->wysiwyg(['width' => 275, 'height' => 100]); ?>
                        </div>

                        <a href="/user/register" class="button red">Start for Free!</a>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
        <div class="clear-fix"></div>
        <div class="wysiwyg">
            <?= $this->wysiwyg('outro', ['width' => 1200, 'height' => 50]); ?>
        </div>
    </div>
</section>