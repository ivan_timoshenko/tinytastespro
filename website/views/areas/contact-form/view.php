<?php if ($this->submitted): ?>
    <p>&nbsp;</p>
    <p style="text-align: center;">
        <strong>Thanks for contacting us!</strong>
    </p>

    <p style="text-align: center;">
        We will be in touch soon.
    </p>
    <p>&nbsp;</p>
<?php else: ?>
    <form action="" method="post" class="form" style="margin: 30px 0;">
        <p class="full-width left">
            <label for="first-name"> 
                <span>First Name</span>
                <input name="first-name" id="first-name" type="text" required="required"/>
            </label>
        </p>

        <p class="full-width right">
            <label for="last-name">
                <span>Last Name</span>
                <input name="last-name" id="last-name" type="text" required="required"/>
            </label>
        </p>

        <p class="full-width left">
            <label for="email">
                <span>Email</span>
                <input name="email" id="email" type="email" required="required" class="email"/>
            </label>
        </p>

        <p class="full-width right">
            <label for="age">
                <span>Child's Age</span> (optional)
                <input name="age" id="age" type="text"/>
            </label>
        </p>

        <p class="full-width">
            <label for="speciality">
                <span>I am</span>
                <select name="speciality" id="speciality">
                    <?php foreach ([
                                       "Clinician",
                                       "Parent",
                                       "Caregiver",
                                       "Other"
                                   ] as $value): ?>
                        <option value="<?= $value; ?>" ><?= $value; ?></option>
                    <?php endforeach; ?>
                </select>
            </label>
        </p>

        <p class="full-width">
            <label for="message">
                <span>Message</span>
                <textarea name="message" id="message" cols="30" rows="5" required="required"></textarea>
            </label>
        </p>

        <p class="full-width" style="text-align: center;">
            <input type="submit" value="Submit"/>
        </p>

        <div class="error-message"><span></span></div>

        <input type="hidden" name="submit" value="contact"/>

    </form>

    <?php $this->placeholder('scripts')->captureStart(); ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('form').validate({
                invalidHandler: function (event, validator) {
                    // 'this' refers to the form
                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        var message = errors == 1
                            ? 'You missed 1 field. It has been highlighted'
                            : 'You missed ' + errors + ' fields. They have been highlighted';
                        $("div.error-message span").html(message);
                        $("div.error-message").show();
                    } else {
                        $("div.error-message").hide();
                    }
                },
                highlight: function (element, errorClass) {
                    $(element).addClass('error');
                    if ($(element).attr('type') == 'file') {
                        $(element).next('label').addClass('error');
                    }
                },
                unhighlight: function (element, errorClass) {
                    $(element).removeClass('error');
                    if ($(element).attr('type') == 'file') {
                        $(element).next('label').removeClass('error');
                    }
                },
                errorPlacement: function (error, element) {
                    return;
                }
            });

            $('#office-phone').mask('(000) 000-0000');
        });
    </script>
    <?php $this->placeholder('scripts')->captureEnd(); ?>
<?php endif; ?>