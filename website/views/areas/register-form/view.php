<form action="<?= $this->document->getFullPath(); ?>" method="post" class="form" style="margin: 30px 0;">
    <?php if ($this->editmode): ?>
        Type: <?= $this->input('type'); ?>
    <?php endif; ?>

    <h2>Personal data</h2>

    <p class="full-width left">
        <label for="first-name">
            <span>First Name</span>
            <input name="first-name" id="first-name" type="text" required="required"/>
        </label>
    </p>

    <p class="full-width right">
        <label for="last-name">
            <span>Last Name</span>
            <input name="last-name" id="last-name" type="text" required="required"/>
        </label>
    </p>

    <p class="full-width left">
        <label for="email">
            <span>Email</span>
            <input name="email" id="email" type="email" required="required" class="email"/>
        </label>
    </p>

    <p class="full-width">
        <label for="password">
            <span>Password:</span>
            <input type="password" name="password" id="password" required="required"/>
        </label>
    </p>

    <p class="full-width">
        <label for="passwordRep">
            <span>Confirm password:</span>
            <input type="password" name="passwordRep" id="passwordRep" required="required"/>
        </label>
    </p>

    <?php if ($this->input('type')->getData() == 'family'): ?>

    <?php elseif ($this->input('type')->getData() == 'professional'): ?>
        <h2></h2>

    <?php endif; ?>

    <p class="full-width" style="text-align: center;">
        <input type="submit" value="Submit"/>
    </p>

    <div class="error-message"><span></span></div>

    <input type="hidden" name="submit" value="register"/>
    <input type="hidden" name="type" value="<?= $this->input('type'); ?>"/>
</form>

<?php $this->placeholder('scripts')->captureStart(); ?>
<script type="text/javascript">
    $(document).ready(function () {
        $('form').validate({
            invalidHandler: function (event, validator) {
                // 'this' refers to the form
                var errors = validator.numberOfInvalids();
                if (errors) {
                    var message = errors == 1
                        ? 'You missed 1 field. It has been highlighted'
                        : 'You missed ' + errors + ' fields. They have been highlighted';
                    $("div.error-message span").html(message);
                    $("div.error-message").show();
                } else {
                    $("div.error-message").hide();
                }
            },
            highlight: function (element, errorClass) {
                $(element).addClass('error');
                if ($(element).attr('type') == 'file') {
                    $(element).next('label').addClass('error');
                }
            },
            unhighlight: function (element, errorClass) {
                $(element).removeClass('error');
                if ($(element).attr('type') == 'file') {
                    $(element).next('label').removeClass('error');
                }
            },
            errorPlacement: function (error, element) {
                return;
            }
        });

        $('#office-phone').mask('(000) 000-0000');
    });
</script>
<?php $this->placeholder('scripts')->captureEnd(); ?>