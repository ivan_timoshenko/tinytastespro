<?php

/**
 * Class PRO_UserController
 *
 * todo redirect to index upon login instead of account/edit
 */
use backendless\Backendless;
use backendless\model\BackendlessUser;
use Pimcore\Tool\Session;
use backendless\exception\BackendlessException;
class PRO_UserController extends PRO_Controller_Public
{
    /**
     * Login form and procession
     */
    public function  loginAction()
    {
        if ($this->session->isActive()){
            $this->redirect('/account/edit');
        } else if ($this->getRequest()->isPost()) {
            try {
                $user = PRO_UserRepository::findOneBy(['email' => $this->getParam('email')]);
                //$user = Backendless::$UserService->login($this->getParam('email'), $this->getParam('password'));
                Session::get("PRO")->user = $user;
                $invitation_id = $this->getParam('id');
                if(!empty($invitation_id)){
                    $this->redirect('/user/accept-' . $this->getParam('type') .'-invitation?id=' . $invitation_id);
                }
                $this->redirect('/account/edit');
                // Do stuff after successful login.
            } catch (BackendlessException $e) {
                // The login failed. Check error to see why.
                $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                    'type' => 'error',
                    'message' => "Couldn't login with error: {$e->getMessage()}. Please contact administrator."
                ]));
                $this->redirect('/user/login');
            }
        }
    }


    /**
     * Renders registration form and processed the registration. Only of clinic managers!
     * @throws Exception
     */
    public function registerAction()
    {
        if ($this->getRequest()->isPost()) {
            $this->view->errors = [];

            if (!$user = PRO_UserRepository::findOneBy(['email' => $this->getParam('email')],[], true)) {
                // User
                $user = new BackendlessUser();
                $user->setProperty('username', $this->getParam('email'));
                $user->setProperty('email', $this->getParam('email'));
                $user->setProperty('password', $this->getParam('password'));
                try {
                    Backendless::$UserService->register($user);
                } catch (BackendlessException $ex) {
                    $this->view->errors[] = 'Couldn\'t create user with such email.';
                }
            } else {
                    $this->view->errors[] = 'Coudn\'t create account - user with such email is already exist.';
            }
            if(!count($this->view->errors)) {
                // user.person.address
                $address = new PRO_AddressRepository();
                $address->set('address', $this->getParam('address'));
                $address->set('city', $this->getParam('city'));
                $address->set('postCode', $this->getParam('postCode'));
                $address->set('country', new PRO_CountryRepository($this->getParam('country')));
                $address->set('state', new PRO_StateRepository($this->getParam('state')));

                // user.person
                $person = new PRO_PersonRepository();
                $person->set('address', $address);
                $person->set('firstName', $this->getParam('firstName'));
                $person->set('lastName', $this->getParam('lastName'));
                $person->set('initials', $this->getParam('initials'));
                $person->set('preferredLanguage', new PRO_LanguageRepository($this->getParam('preferredLanguage')));
            }
            if(!count($this->view->errors)) {
                $user = Backendless::$UserService->login($this->getParam('email'), $this->getParam('password'));
                $user->setProperty('person', $person);
                // save user
                try {
                    Backendless::$UserService->update($user);
                } catch (BackendlessException $ex) {
                    $this->view->errors[] = "Person couldn't be saved.";
                }
            }
            if(!count($this->view->errors)) {
                // professional
                $professional = new PRO_ProfessionalRepository();
                $professional->set('speciality', $this->getParam('speciality'));
                $professional->set('professional', new PRO_UserRepository($user->getObjectId()));
                // professional.clinic
                $professional->set('clinic', new PRO_ClinicRepository());
                $professional->get('clinic')->set('name', $this->getParam('clinicName'));
                $professional->get('clinic')->set('admin', new PRO_UserRepository($user->getObjectId()));
                // professional.clinic.address
                $professional->get('clinic')->set('address', new PRO_AddressRepository());
                $professional->get('clinic')->get('address')->set('address', $this->getParam('address'));
                $professional->get('clinic')->get('address')->set('city', $this->getParam('city'));
                $professional->get('clinic')->get('address')->set('postCode', $this->getParam('postCode'));
                $professional->get('clinic')->get('address')->set('country', new PRO_CountryRepository($this->getParam('country')));
                $professional->get('clinic')->get('address')->set('state', new PRO_StateRepository($this->getParam('state')));
                $professional->get('clinic')->get('address')->set('phone', $this->getParam('phoneNumber'));
                // save professional
                try {
                    $professional->save();
                } catch (BackendlessException $ex) {
                    $this->view->errors[] = 'Professional record couldn\'t be saved.';
                }
            }
            if(!count($this->view->errors)) {
                // create subscription
                $subscription = new PRO_SubscriptionRepository();
                $subscription->set('customer', new PRO_UserRepository($user->getObjectId()));
                $subscription->set('clinic', new PRO_ClinicRepository($professional->get('clinic')->getObjectId()));
                $subscription->set('valid', false);
                // todo carry tariff over get query instead of picking maximum
                $subscription->set('type', new PRO_SubscriptionTypeRepository(PRO_SubscriptionTypeRepository::findOneBy(['maxPatients' => 100, 'professional' => true])->getObjectId()));
                // save professional
                try {
                    $subscription->save();
                } catch (BackendlessException $ex) {
                    $this->view->errors[] = 'Subscription couldn\'t be saved.';
                }
            }
            if (!count($this->view->errors)) {
                // Redirect
                $this->redirect('/account/edit');
            }
        }

        // Prepare options for selects
        $this->view->options = new stdClass();
        $this->view->options->country = PRO_CountryRepository::findBy([], ['orderBy' => 'name', 'limit' => 999]);
        $this->view->options->state = PRO_StateRepository::findBy([], ['orderBy' => 'name', 'limit' => 100]);
        $this->view->options->language = PRO_LanguageRepository::findBy([], ['orderBy' => 'name']);
    }


    public function registerFamilyAction()
    {
        if ($this->getRequest()->isPost()) {
            $this->view->errors = [];
            if (!$user = PRO_UserRepository::findOneBy(['email' => $this->getParam('email')],[], true)) {
                // User
                $user = new BackendlessUser();
                $user->setProperty('username', $this->getParam('email'));
                $user->setProperty('email', $this->getParam('email'));
                $user->setProperty('password', $this->getParam('password'));
                try {
                    $user = Backendless::$UserService->register($user);
                } catch (BackendlessException $ex) {
                    $this->view->errors[] = 'Couldn\'t create user with such email.';
                }
            } else {
                $this->view->errors[] = 'Coudn\'t create account - user with such email is already exist.';
            }
            if(!count($this->view->errors)) {
                // user.person.address
                $address = new PRO_AddressRepository();
                $address->set('address', $this->getParam('address'));
                $address->set('city', $this->getParam('city'));
                $address->set('postCode', $this->getParam('postCode'));
                $address->set('country', new PRO_CountryRepository($this->getParam('country')));
                $address->set('state', new PRO_StateRepository($this->getParam('state')));
                try {
                    $address->save();
                } catch (BackendlessException $ex) {
                    $this->view->errors[] = "Address couldn't be saved.";
                }
            }
            if(!count($this->view->errors)) {
                // user.person
                $person = new PRO_PersonRepository();
                $person->set('address', new PRO_AddressRepository($address->getObjectId()));
                $person->set('firstName', $this->getParam('firstName'));
                $person->set('lastName', $this->getParam('lastName'));
                $person->set('initials', $this->getParam('initials'));
                $person->set('initials', $this->getParam('initials'));
                $person->set('preferredLanguage', new PRO_LanguageRepository($this->getParam('preferredLanguage')));
                try {
                    $person->save();
                } catch (BackendlessException $ex) {
                    $this->view->errors[] = "Person couldn't be saved.";
                }
            }
            if(!count($this->view->errors)) {
                $user = Backendless::$UserService->login($this->getParam('email'), $this->getParam('password'));
                $user->setProperty('person', new PRO_PersonRepository($person->getObjectId()));
                // save user
                try {
                    Backendless::$UserService->update($user);
                } catch (BackendlessException $ex) {
                    $this->view->errors[] = "Users.person couldn't be saved.";
                }
            }
            if(!count($this->view->errors)) {
                // familyMember.family
                $family = new PRO_FamilyRepository();
                $family->set('admin', new PRO_UserRepository($user->getObjectId()));
                try {
                    $family->save();
                } catch (BackendlessException $ex) {
                    $this->view->errors[] = "Family couldn't be saved.";
                }
            }
            if(!count($this->view->errors)) {
                // familyMember
                $familyMember = new PRO_FamilyMemberRepository();
                $familyMember->set('member', new PRO_UserRepository($user->getObjectId()));
                $familyMember->set('family', new PRO_FamilyRepository($family->getObjectId()));
                try {
                    $familyMember->save();
                } catch (BackendlessException $ex) {
                    $this->view->errors[] = "FamilyMember couldn't be saved.";
                }
            }
            if(!count($this->view->errors)) {
                // subscription
                $subscription = new PRO_SubscriptionRepository();
                $subscription->set('customer', new PRO_UserRepository($user->getObjectId()));
                $subscription->set('family', new PRO_FamilyRepository($family->getObjectId()));
                $subscription->set('valid', false);
                $subscription->set('type', new PRO_SubscriptionTypeRepository(PRO_SubscriptionTypeRepository::findOneBy(['maxPatients' => 1, 'professional' => false])->getObjectId()));
                try {
                    $subscription->save();
                } catch (BackendlessException $ex) {
                    $this->view->errors[] = "Subscription couldn't be saved.";
                }
            }
            if (!count($this->view->errors)) {
                //Login
                $user = Backendless::$UserService->login($this->getParam('email'), $this->getParam('password'));
                Session::get("PRO")->user = $user;
                // Redirect
                $this->redirect('/account/edit');
            }
        }

        // Prepare options for selects
        $this->view->options = new stdClass();
        $this->view->options->country = PRO_CountryRepository::findBy([], ['orderBy' => 'name', 'limit' => 999]);
        $this->view->options->state = PRO_StateRepository::findBy([], ['orderBy' => 'name', 'limit' => 100]);
        $this->view->options->language = PRO_LanguageRepository::findBy([], ['orderBy' => 'name']);
    }


    /**
     * Logout action
     */
    public function logoutAction()
    {
        Backendless::$UserService->logout(Session::get("PRO")->user);
        Session::get("PRO")->user = null;
        $this->redirect('/user/login');// todo rework to use route
    }

    /**
     * Logout action
     */
    public function forgotPasswordAction()
    {
        if ($this->session->isActive()){
            $this->redirect('/account/edit');
        } else if ($this->getRequest()->isPost()) {
            try {
                $user = Backendless::$UserService->restorePassword($this->getParam('email'));
            } catch (BackendlessException $e) {
                $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                    'type' => 'error',
                    'message' => "Couldn't restore password with error: {$e->getMessage()}. Please contact administrator."
                ]));
                $this->redirect('/user/login');
            }
        }
    }

    /**
     * Accept the invitation as a family member. Also renders password generation form.
     * todo manipulate family subscription
     * todo be shure that errors is rendered if something is going wrong
     * @throws Exception
     * @throws Zend_Controller_Router_Exception
     */
    public function acceptFamilyInvitationAction()
    {
        if (!$invitation = PRO_InvitationRepository::find($this->getParam('id'), $noCache = true)) {
            throw new Zend_Controller_Router_Exception("Unknown invitation");
        }

        if ($invitation->registeredUser && !$this->session->getUser()) {
            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'error',
                'message' => 'Please first login to use this invitation'
            ]));
            $this->redirect('/user/login?id=' . $this->getParam('id') .'&type=family');
        }


        if ($invitation->registeredUser) {
            if($invitation->registeredUser->getObjectId() != $this->session->getUser()->getObjectId()){
                $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                    'type' => 'error',
                    'message' => 'Please first login in the correct account to use this invitation'
                ]));
                $this->redirect('/account/edit');
            }

            if(!count($this->view->errors)) {
                // Assign as caretaker to inviter's children
            $children = PRO_CaretakerRepository::getChildren($invitation->inviter->person);

            foreach ($children as $child) {
                $caretaker = new PRO_CaretakerRepository();
                $caretaker->set('caretaker', new PRO_PersonRepository($this->session->getPerson()->getObjectId()));
                $caretaker->set('child', new PRO_PersonRepository($child->getObjectId()));
                $caretaker->set('primary', false);
                try {
                    $caretaker->save();
                } catch (BackendlessException $ex) {
                    $this->view->errors[] = "Caretaker couldn't be saved.";
                }
            }
        }
            if(!count($this->view->errors)) {
                // Assign to inviter's administrated families
                foreach (PRO_FamilyMemberRepository::findBy(['member.objectId' => $invitation->inviter->getObjectId()]) as $familyMember) {
                    $member = new PRO_FamilyMemberRepository();
                    $member->set('family', new PRO_FamilyRepository($familyMember->get('family')->getObjectId()));
                    $member->set('member', new PRO_UserRepository($this->session->getUser()->getObjectId()));
                    try {
                        $member->save();
                    } catch (BackendlessException $ex) {
                        $this->view->errors[] = "Member couldn't be saved.";
                    }
                }
            }
            if(!count($this->view->errors)) {
                // Update invitation record
                $invitation->set('accepted', date('m/d/Y H:i O'));
                $invitation->set('caretaker', true);
                $invitation->set('registeredUser', new PRO_UserRepository($this->session->getUser()->getObjectId()));
                try {
                    $invitation->save();
                } catch (BackendlessException $ex) {
                    $this->view->errors[] = "Invitation couldn't be saved.";
                }
            }

            if(!empty($invitation->family['objectId'])) {
                // clear cache for family
                PRO_InvitationRepository::purgeCollectionCache(['family.objectId' => $invitation->family['objectId'], 'caretaker' => true]);
            } elseif ( $invitation->clinic['objectId']){
                // clear cache for clinic
                PRO_InvitationRepository::purgeCollectionCache(['clinic.objectId' => $invitation->clinic['objectId'], 'caretaker' => true]);
            }
            //own cache for proper family retrieval
            PRO_InvitationRepository::purgeCollectionCache(['registeredUser.objectId' => $this->session->getUser()->getObjectId()]);

            if(!count($this->view->errors)) {
                $invitation->set('accepted', date('m/d/Y H:i O'));
                try {
                    $invitation->save();
                } catch (BackendlessException $ex) {
                    $this->view->errors[] = "Invitation couldn't be saved.";
                }
            }
            if(!count($this->view->errors)) {
                $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                    'type' => 'success',
                    'message' => 'Successfully accepted invitation'
                ]));
                $this->redirect('/account/edit');
            } else {
                $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                    'type' => 'error',
                    'message' => 'Error'
                ]));
                $this->redirect('/account/edit');
            }
        }

        if ($this->getRequest()->isPost()) {
            // User
            $user = new BackendlessUser();
            $user->setProperty('username', $invitation->get('email'));
            $user->setProperty('email', $invitation->get('email'));
            $user->setProperty('password', $this->getParam('password'));

            try {
                Backendless::$UserService->register($user);
            } catch (BackendlessException $ex) {
                $this->view->errors[] = 'Couldn\'t create user with such email.';
            }

            if(!count($this->view->errors)) {
                // user.person
                $person = new PRO_PersonRepository();
                $person->set('firstName', $this->getParam('firstName'));
                $person->set('lastName', $this->getParam('lastName'));

                $user = Backendless::$UserService->login($invitation->get('email'), $this->getParam('password'));
                $user->setProperty('person', $person);
                // save user
                try {
                    Backendless::$UserService->update($user);
                } catch (BackendlessException $ex) {
                    $this->view->errors[] = 'Person couldn\'t be saved.';
                }
            }
            if(!count($this->view->errors)) {
                // Assign as caretaker to inviter's children
                $children = PRO_CaretakerRepository::getChildren($invitation->inviter->person);
                foreach ($children as $child) {
                    $caretaker = new PRO_CaretakerRepository();
                    $caretaker->set('caretaker', new PRO_PersonRepository($user->person['objectId']));
                    $caretaker->set('child', new PRO_PersonRepository($child->getObjectId()));
                    $caretaker->set('primary', false);
                    try {
                        $caretaker->save();

                    } catch (BackendlessException $ex) {
                        $this->view->errors[] = "Caretaker couldn't be saved.";
                    }
                }
            }
            if(!count($this->view->errors)) {
                // Assign to inviter's administrated families
                foreach (PRO_FamilyMemberRepository::findBy(['member.objectId' => $invitation->inviter->getObjectId()]) as $familyMember) {
                    $member = new PRO_FamilyMemberRepository();
                    $member->set('family', new PRO_FamilyRepository($familyMember->get('family')->getObjectId()));
                    $member->set('member', new PRO_UserRepository($user->getObjectId()));
                    try {
                        $member->save();
                    } catch (BackendlessException $ex) {
                        $this->view->errors[] = "Member couldn't be saved.";
                    }
                }
            }
            if(!count($this->view->errors)) {
                // Update invitation record
                $invitation->set('accepted', date('m/d/Y H:i O'));
                $invitation->set('caretaker', true);
                $invitation->set('registeredUser', new PRO_UserRepository($user->getObjectId()));
                try {
                    $invitation->save();
                } catch (BackendlessException $ex) {
                    $this->view->errors[] = "Invitation couldn't be saved.";
                }
            }

            if(!empty($invitation->family['objectId'])) {
                // clear cache for family
                PRO_InvitationRepository::purgeCollectionCache(['family.objectId' => $invitation->family['objectId'], 'caretaker' => true]);
            } elseif ( $invitation->clinic['objectId']){
                // clear cache for clinic
                PRO_InvitationRepository::purgeCollectionCache(['clinic.objectId' => $invitation->clinic['objectId'], 'caretaker' => true]);
            }
            if($this->session->getUser())
                $session_user = $this->session->getUser()->getObjectId();
            if(!empty($session_user)){
                PRO_InvitationRepository::purgeCollectionCache(['registeredUser.objectId' => $session_user]);
            }
            //own cache for proper family retrieval


            if(!count($this->view->errors)) {
                Session::get("PRO")->user = $user;
                $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                    'type' => 'success',
                    'message' => 'Successfully accepted invitation'
                ]));
                // Redirect
                $this->redirect('/account/edit');// todo rework to use route
            }
        }

        $this->view->invitation = $invitation;
    }


    /**
     * @throws Exception
     * @throws Zend_Controller_Router_Exception
     * todo be shure that errors is rendered if something is going wrong
     */
    public function acceptClinicInvitationAction()
    {
        if (!$invitation = PRO_InvitationRepository::find($this->getParam('id'), $noCache = true)) {
            throw new Zend_Controller_Router_Exception("Unknown invitation");
        }

        if ($invitation->registeredUser ) {

            if (!$user = $this->session->getUser()){
                $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                    'type' => 'error',
                    'message' => 'Please first login to use this invitation'
                ]));
                $this->redirect('/user/login?id=' . $this->getParam('id') .'&type=clinic');
            } else {
                if ($invitation->registeredUser->getObjectId() !== $this->session->getUser()->getObjectId()) {
                    throw new Zend_Controller_Router_Exception("Unknown invitation");
                }

                $invitations = PRO_InvitationRepository::findBy(['email' => $invitation->email],[],true);

                foreach ($invitations as $invitation_now){
                    if($invitation_now->getObjectId() != $invitation->getObjectId()){
                        $invitation_now->destroy();
                        PRO_InvitationRepository::purgeCollectionCache(['clinic.objectId' => $invitation_now->clinic->objectId, 'professional' => true]);
                        PRO_InvitationRepository::purgeCollectionCache(['inviter.objectId' => $invitation_now->inviter->objectId, 'professional' => true]);
                    }
                }
                // clear cache for clinic user

                // professional
                $professional = new PRO_ProfessionalRepository();
                $professional->set('clinic', new PRO_ClinicRepository($invitation->get('clinic')->getObjectId()));
                $professional->set('professional', new PRO_UserRepository($user->getObjectId()));
                try {
                    $professional->save();
                } catch (BackendlessException $ex) {
                    $this->view->errors[] = "Professional couldn't be saved.";
                }
                if(!count($this->view->errors)) {
                    // Update invitation record
                    $invitation->set('accepted', date('m/d/Y H:i O'));
                    $invitation->set('caretaker', false);
                    try {
                        $invitation->save();
                    } catch (BackendlessException $ex) {
                        $this->view->errors[] = "Invitation couldn't be saved.";
                    }
                }
                PRO_InvitationRepository::purgeCollectionCache(['clinic.objectId' => $invitation->clinic['objectId'], 'professional' => true]);
                // clear cache for inviter
                PRO_InvitationRepository::purgeCollectionCache(['inviter.objectId' => $invitation->inviter->objectId, 'professional' => true]);
                //own cache for proper clinic retrieval
                $session_user = $this->session->getUser()->getObjectId();
                if(!empty($session_user)){
                    PRO_InvitationRepository::purgeCollectionCache(['registeredUser.objectId' => $session_user]);
                }
                if(!count($this->view->errors)) {
                    Session::get("PRO")->user = $user;
                    $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                        'type' => 'success',
                        'message' => 'Successfully accepted invitation'
                    ]));
                    // Redirect
                    $this->redirect('/account/edit');// todo rework to use route
                }
            }
        }

        if ($this->getRequest()->isPost()) {
            // User
            $user = new BackendlessUser();
            $user->setProperty('username', $invitation->get('email'));
            $user->setProperty('email', $invitation->get('email'));
            $user->setProperty('password', $this->getParam('password'));

            try {
                Backendless::$UserService->register($user);
            } catch (BackendlessException $ex) {
                $this->view->errors[] = 'Couldn\'t create user with such email.';
            }
            $invitations = PRO_InvitationRepository::findBy(['email' => $invitation->email],[],true);

            foreach ($invitations as $invitation_now){
                if($invitation_now->getObjectId() != $invitation->getObjectId()){
                    $invitation_now->destroy();
                    PRO_InvitationRepository::purgeCollectionCache(['clinic.objectId' => $invitation_now->clinic->objectId, 'professional' => true]);
                    PRO_InvitationRepository::purgeCollectionCache(['inviter.objectId' => $invitation_now->inviter->objectId, 'professional' => true]);
                }
            }
            if(!count($this->view->errors)) {
                // user.person
                $person = new PRO_PersonRepository();
                $person->set('firstName', $invitation->get('firstName'));
                $person->set('lastName', $invitation->get('lastName'));

                $user = Backendless::$UserService->login($invitation->get('email'), $this->getParam('password'));
                $user->setProperty('person', $person);
                // save user
                try {
                    Backendless::$UserService->update($user);
                } catch (BackendlessException $ex) {
                    $this->view->errors[] = 'Person couldn\'t be saved.';
                }
            }
            if(!count($this->view->errors)) {
                // professional
                $professional = new PRO_ProfessionalRepository();
                $professional->set('clinic', new PRO_ClinicRepository($invitation->get('clinic')->getObjectId()));
                $professional->set('professional', new PRO_UserRepository($user->getObjectId()));
                try {
                    $professional->save();
                } catch (BackendlessException $ex) {
                    $this->view->errors[] = "Professional couldn't be saved.";
                }
            }
            if(!count($this->view->errors)) {
                // Update invitation record
                $invitation->set('accepted', date('m/d/Y H:i O'));
                $invitation->set('caretaker', false);
                $invitation->set('registeredUser', new PRO_UserRepository($user->getObjectId()));
                try {
                    $invitation->save();
                } catch (BackendlessException $ex) {
                    $this->view->errors[] = "Invitation couldn't be saved.";
                }
            }
            PRO_InvitationRepository::purgeCollectionCache(['clinic.objectId' => $invitation->clinic['objectId'], 'professional' => true]);
            // clear cache for inviter
            PRO_InvitationRepository::purgeCollectionCache(['inviter.objectId' => $invitation->inviter->objectId, 'professional' => true]);
            //own cache for proper clinic retrieval
            if($this->session->getUser())
                $session_user = $this->session->getUser()->getObjectId();
            if(!empty($session_user)){
                PRO_InvitationRepository::purgeCollectionCache(['registeredUser.objectId' => $session_user]);
            }
            if(!count($this->view->errors)) {
                Session::get("PRO")->user = $user;
                $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                    'type' => 'success',
                    'message' => 'Successfully accepted invitation'
                ]));
                // Redirect
                $this->redirect('/account/edit');// todo rework to use route
            }
        }
        $this->view->invitation = $invitation;
    }


    /**
     * @throws Exception
     * @throws Zend_Controller_Router_Exception
     * todo be shure that errors is rendered if something is going wrong
     */
    public function  acceptFamilyToClinicInvitationAction()
    {
        if (!$invitation = PRO_InvitationRepository::find($this->getParam('id'), $noCache = true)) {
            throw new Zend_Controller_Router_Exception("Unknown invitation");
        }

        if ($invitation->registeredUser ) {
            if (!$this->session->getUser()){
                $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                    'type' => 'error',
                    'message' => 'Please first login to use this invitation'
                ]));
                $this->redirect('/user/login?id=' . $this->getParam('id') .'&type=family-to-clinic');
            } else {
                if ($invitation->registeredUser->getObjectId() !== $this->session->getUser()->getObjectId()) {
                    throw new Zend_Controller_Router_Exception("Unknown invitation");
                }

                // clear cache for clinic user
                PRO_InvitationRepository::purgeCollectionCache(['clinic.objectId' => $invitation->clinic['objectId'], 'caretaker' => true]);
                //own cache for proper clinic retrieval
                PRO_InvitationRepository::purgeCollectionCache(['registeredUser.objectId' => $this->session->getUser()->getObjectId()]);

                // Create ProfessionalFollow record
                $professionalFollow = new PRO_ProfessionalFollowRepository();
                $professionalFollow->set('clinic', new PRO_ClinicRepository($invitation->clinic['objectId']));
                $professionalFollow->set('professional', new PRO_UserRepository($invitation->inviter->getObjectId()));
                $professionalFollow->set('approved', new PRO_UserRepository($this->session->getUser()->getObjectId()));
                $professionalFollow->set('caretaker', new PRO_UserRepository($this->session->getUser()->getObjectId()));
                $professionalFollow->set('start', date('m/d/Y H:i O'));
                try {
                    $professionalFollow->save();
                } catch (BackendlessException $ex) {
                    $this->view->errors[] = "ProfessionalFollow couldn't be saved.";
                }

                if (!count($this->view->errors)) {
                    $invitation->set('accepted', date('m/d/Y H:i O'));
                    try {
                        $invitation->save();
                    } catch (BackendlessException $ex) {
                        $this->view->errors[] = "Invitation couldn't be saved.";
                    }
                }
                if (!count($this->view->errors)) {
                    $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                        'type' => 'success',
                        'message' => 'Successfully accepted invitation'
                    ]));
                    $this->redirect('/account/edit');
                }
            }
        }

        if ($this->getRequest()->isPost()) {
            // User
            $user = new BackendlessUser();
            $user->setProperty('username', $invitation->get('email'));
            $user->setProperty('email', $invitation->get('email'));
            $user->setProperty('password', $this->getParam('password'));

            try {
                Backendless::$UserService->register($user);
            } catch (BackendlessException $ex) {
                $this->view->errors[] = 'Couldn\'t create user with such email.';
            }
            if(!count($this->view->errors)) {
                // user.person
                $person = new PRO_PersonRepository();
                $person->set('firstName', $invitation->get('firstName'));
                $person->set('lastName', $invitation->get('lastName'));

                $user = Backendless::$UserService->login($invitation->get('email'), $this->getParam('password'));
                $user->setProperty('person', $person);
                // save user
                try {
                    Backendless::$UserService->update($user);
                } catch (BackendlessException $ex) {
                    $this->view->errors[] = 'Person couldn\'t be saved.';
                }
            }
            if(!count($this->view->errors)) {
                // create family for user
                $familyMember = new PRO_FamilyMemberRepository();
                $familyMember->set('member', new PRO_UserRepository($user->getObjectId()));
                $familyMember->set('family', new PRO_FamilyRepository());
                $familyMember->get('family')->set('admin', new PRO_UserRepository($user->getObjectId()));
                // todo wrap in try
                try {
                    $invitation->save();
                } catch (BackendlessException $ex) {
                    $this->view->errors[] = "FamilyMember couldn't be saved.";
                }
            }
            if(!count($this->view->errors)) {
                // Create ProfessionalFollow record
                $professionalFollow = new PRO_ProfessionalFollowRepository();
                $professionalFollow->set('clinic', new PRO_ClinicRepository($invitation->clinic['objectId']));
                $professionalFollow->set('professional', new PRO_UserRepository($invitation->inviterinviter['objectId']));
                $professionalFollow->set('approved', new PRO_UserRepository($user->getObjectId()));
                $professionalFollow->set('caretaker', new PRO_UserRepository($user->getObjectId()));
                $professionalFollow->set('start', date('m/d/Y H:i O'));
                // todo wrap in try
                try {
                    $professionalFollow->save();
                } catch (BackendlessException $ex) {
                    $this->view->errors[] = "ProfessionalFollow couldn't be saved.";
                }
            }
            if (!count($this->view->errors)) {
                $invitation->set('confirmed', true);
                $invitation->set('accepted', date('m/d/Y H:i O'));
                $invitation->set('registeredUser', new PRO_UserRepository( $user->getObjectId()));
                $invitation->save();

                Session::get("PRO")->user = $user;
                // Redirect
                $this->redirect('/account/edit');
            }
        }
    }
}