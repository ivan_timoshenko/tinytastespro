<?php

class PRO_IpnController extends PRO_Controller_Public
{

    /*
     * Handles incoming IPN requests.
     * todo check and prolong depended records as well (records with parent pointing to current)
     */
    public function ipnAction()
    {
        $this->_helper->viewRenderer->setNoRender(true);
        //$fl = fopen(PIMCORE_DOCUMENT_ROOT . '/paypal_ipn.log', 'a+');

        if ($this->getRequest()->isPost() && $this->getParam('txn_type')) {
            if ($this->getParam('txn_type') == 'subscr_payment') {
                if ($this->verifyPaypalIPN($_POST)) {
                    $subscriptionId = $this->getParam('custom');

                    if ($subscriptionId) {
                        $transactionQuery = new \Parse\ParseQuery("SubscriptionTransaction");
                        $transactionQuery->equalTo("ipnTrackId", $this->getParam('ipn_track_id'));
                        try {
                            $transaction = $transactionQuery->find();
                        } catch (ParseException $ex) {
                            $transaction = 1;
                        }

                        if (!$transaction) {
                            // if this is the first attempt of the IPN deliver of this specific txn_id,
                            // we update the subscription accordingly and log the transaction as well.

                            $subscriptionQuery = new \Parse\ParseQuery("Subscription");
                            try {
                                $subscription = $subscriptionQuery->get($subscriptionId);
                            } catch (\Parse\ParseException $ex) {
                                // todo cease execution at this point
                            }

                            $subscription->set("endDate", date('m/d/Y H:i O'));
                            $subscription->set("startDate", date('m/d/Y H:i O', strtotime('+1 month')));
                            $subscription->set("valid", true);
                            try {
                                $subscription->save();
                            } catch (\Parse\ParseException $ex) {
                            }

                            // clear cache
                            if ($subscription->get('clinic')) {
                                PRO_SubscriptionRepository::purgeCollectionCache(['clinic' => $subscription->get('clinic')]);
                            }
                            if ($subscription->get('family')) {
                                PRO_SubscriptionRepository::purgeCollectionCache(['family' => $subscription->get('family')]);
                            }

                            try {
                                $subscriptionTx = new \Parse\ParseObject("SubscriptionTransaction");
                                $subscriptionTx->set("ipnTrackId", $this->getParam('ipn_track_id'));
                                $subscriptionTx->set("subscription", $subscription);
                                $subscriptionTx->set("ipnData", serialize($_POST));
                                $subscriptionTx->save();
                            } catch (\Parse\ParseException $ex) {
                            }

                        }
                        exit;
                    }

                } else {
                    // Invalid IPN packet -- someone attempted a forgery
                    $this->redirect('/account/edit');
                }
            }
        } else {
            $this->redirect('/account/edit');
        }
    }


    private function verifyPaypalIPN($IPN)
    {
        if (empty($IPN['verify_sign'])) {
            return null;
        }
        $IPN['cmd'] = '_notify-validate';
        $paypalHost = (empty($IPN['test_ipn']) ? 'www' : 'www.sandbox') . '.paypal.com';
        $cURL = curl_init();
        curl_setopt($cURL, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($cURL, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($cURL, CURLOPT_URL, "https://{$paypalHost}/cgi-bin/webscr");
        curl_setopt($cURL, CURLOPT_ENCODING, 'gzip');
        curl_setopt($cURL, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($cURL, CURLOPT_POST, true); // POST back
        curl_setopt($cURL, CURLOPT_POSTFIELDS, $IPN); // the $IPN
        curl_setopt($cURL, CURLOPT_HEADER, false);
        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cURL, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($cURL, CURLOPT_FORBID_REUSE, true);
        curl_setopt($cURL, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($cURL, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($cURL, CURLOPT_TIMEOUT, 10);
        curl_setopt($cURL, CURLINFO_HEADER_OUT, true);
        curl_setopt($cURL, CURLOPT_HTTPHEADER, array(
            'Connection: close',
            'Expect: ',
        ));
        $response = curl_exec($cURL);
        $status = (int)curl_getinfo($cURL, CURLINFO_HTTP_CODE);
        curl_close($cURL);
        if (empty($response) or !preg_match('~^(VERIFIED|INVALID)$~i', $response = trim($response)) or !$status) {
            return null;
        }
        if (intval($status / 100) != 2) {
            return false;
        }
        return !strcasecmp($response, 'VERIFIED');
    }
}

	