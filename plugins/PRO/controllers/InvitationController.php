<?php

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 3/22/15
 * Time: 6:35 PM
 */

use backendless\exception\BackendlessException;
class PRO_InvitationController extends PRO_Controller_Protected
{

    /**
     * Invites non-user to join clinic as a non-manager
     * @throws Exception
     */
    public function inviteClinicMemberAction()
    {
        //todo you shouldn't be able to invite yourself
        // Check email is in use
        if ($user = PRO_UserRepository::findOneBy(['email' => $this->getParam('email')])) {
            if ($this->session->
                getUser()->getObjectId() == $user->getObjectId()) {
                $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                    'type' => 'error',
                    'message' => 'You can\'t invite yourself'
                ]));
                $this->redirect('/account/edit');
            }

            $professional_check = PRO_ProfessionalRepository::findOneBy(['professional.objectId' => $user->getObjectId()], [], true);
            $familyMember = PRO_FamilyMemberRepository::findOneBy(['member.objectId' => $user->getObjectId()], [], true);
            if(!empty($familyMember)){
                $family = PRO_FamilyRepository::findOneBy(['objectId' => $familyMember->get('family')->getObjectId()], [] ,true);
            }

            if(!empty($professional_check)){
                $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                    'type' => 'error',
                    'message' => 'User with such email is already in another clinic'
                ]));
                $this->redirect('/account/edit');
            }
            if(!empty($family)){
                if($family->get('admin')->getObjectId() == $user->getObjectId()){
                    $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                        'type' => 'error',
                        'message' => 'User with such email is family manager'
                    ]));
                    $this->redirect('/account/edit');
                }
            }


            if (PRO_InvitationRepository::findOneBy(['email' => $this->getParam('email'), 'clinic.objectId' => $this->session->getClinic()->getObjectId()])) {
                $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                    'type' => 'error',
                    'message' => 'User with such email is already in your clinic'
                ]));
                $this->redirect('/account/edit');
            }
        }
        $invitation = new PRO_InvitationRepository();
        if($user)
            $invitation->set('registeredUser', new PRO_UserRepository($user->getObjectId()));
        $invitation->set('email', $this->getParam('email'));
        $invitation->set('firstName', $this->getParam('firstName'));
        $invitation->set('lastName', $this->getParam('lastName'));
        $invitation->set('professional', true);
        $invitation->set('clinic', new PRO_ClinicRepository ( $this->session->getClinic()->getObjectId() ));
        $invitation->set('inviter', new PRO_UserRepository( $this->session->getUser()->getObjectId()));

        // Save invitation
        try {
            $invitation->save();
        } catch (\Exception $e) {
            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'error',
                'message' => "Couldn't invite with error: {$e->getMessage()}. Please contact administrator."
            ]));
            $this->redirect('/account/edit');
        }

        // Clear cache
        PRO_InvitationRepository::purgeCollectionCache(['clinic.objectId' => $this->session->getClinic()->getObjectId(), 'professional' => true]);

        // Send an email to invited person
        $mail = new Pimcore_Mail();
        $mail->addTo($invitation->get('email'));
        // Collect invitation with all referred fields. Remember, that for saving purpose we only set up references instead of actual fields
        $this->view->invitation = PRO_InvitationRepository::findOneBy(['objectId' =>$invitation->getObjectId()]);
        $this->view->inviter = $this->session->getPerson();
        $content = $this->view->render('email/invite-clinic-member.php');
        $mail->setSubject('Important Taste Invitation');
        $mail->setBodyHtml($content);
        try {
            $mail->send();
        } catch (\Exception $e) {
            echo $mail->getBodyHtmlRendered();
            exit();
        }

        $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
            'type' => 'success',
            'message' => 'Successfully invited person to become clinic employee'
        ]));
        $this->redirect('/account/edit');
    }


    /**
     * Invites a person to be governed by clinic. Can both invite existing and non-existing users
     * todo validate that subscription allows to invite even more patients
     * todo validate user doesn't already belong to clinic
     * @throws Exception
     */
    public function inviteFamilyToClinicAction()
    {
        // Check invitations
        //todo you shouldn't be able to invite yourself
        if ($user = PRO_UserRepository::findOneBy(['email' => $this->getParam('email')], [],true)) {

            if ($this->session->getUser()->getObjectId() == $user->getObjectId()) {
                $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                    'type' => 'error',
                    'message' => 'You can\'t invite yourself'
                ]));
                $this->redirect('/account/edit');
            }

            $familyMember = PRO_FamilyMemberRepository::findOneBy(['member.objectId' => $user->getObjectId()]);
            if(!empty($familyMember)){
                $family = PRO_FamilyRepository::findOneBy(['objectId' => $familyMember->get('family')->getObjectId()]);
            }
            if(!empty($family)){
                if($family->get('admin')->getObjectId() != $user->getObjectId()){
                    $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                        'type' => 'error',
                        'message' => 'User with such email is no family manager'
                    ]));
                    $this->redirect('/account/edit');
                }
            } else {
                $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                    'type' => 'error',
                    'message' => 'User with such email is no family manager'
                ]));
                $this->redirect('/account/edit');
            }
        }

        // Check for free slots
        if (PRO_InvitationRepository::countBy(['clinic.objectId' => $this->session->getClinic()->getObjectId(), 'caretaker' => true]) >= $this->session->getSubscription()->get('type')->get('maxPatients')) {
            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'error',
                'message' => 'You clinic\'s subscription doesn\'t allow more patients. Please consult with your clinic manager.'
            ]));
            $this->redirect('/account/edit');
        }

        $invitation = new PRO_InvitationRepository();
        $invitation->set('email', $this->getParam('email'));
        $invitation->set('firstName', $this->getParam('firstName'));
        $invitation->set('lastName', $this->getParam('lastName'));
        $invitation->set('caretaker', true);
        $invitation->set('clinic', new PRO_ClinicRepository ( $this->session->getClinic()->getObjectId() ));
        $invitation->set('inviter', new PRO_UserRepository( $this->session->getUser()->getObjectId()));
        $invitation->set('email', $this->getParam('email'));

        // If this is an invitation to existing user, we need to indicate this fact in invitation
        if ($user) {
            // Found user was not invited by anyone (so it cannot be family non-manager), otherwise Invitation check would fail.
            // todo add extensive check that user is not family non-manager
            // It means this user can either be a family manager or clinic manager
            // todo add a check that invited person is not clinic manager
            $invitation->set('registeredUser', new PRO_UserRepository($user->getObjectId()));
            $invitation->set('family', new PRO_FamilyRepository( PRO_FamilyMemberRepository::findOneBy(['member.objectId' => $user->getObjectId()])->get('family')->getObjectId()));
            $invitation->set('firstName', $user->get('person')->get('firstName'));
            $invitation->set('lastName', $user->get('person')->get('lastName'));
        }

        // Save invitation
        try {
            $invitation->save();
        } catch (\Exception $e) {
            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'error',
                'message' => "Couldn't invite with error: {$e->getMessage()}. Please contact administrator."
            ]));
            $this->redirect('/account/edit');
        }

        // Clear cache
        PRO_InvitationRepository::purgeCollectionCache(['clinic.objectId' => $this->session->getClinic()->getObjectId(), 'caretaker' => true]);
        PRO_InvitationRepository::purgeCollectionCache(['inviter.objectId' => $this->session->getUser()->getObjectId(), 'caretaker' => true]);

        // Send an email to invited person
        $mail = new Pimcore_Mail();
        $mail->addTo($invitation->get('email'));
        // Collect invitation with all referred fields. Remember, that for saving purpose we only set up references instead of actual fields
        $this->view->invitation = PRO_InvitationRepository::findOneBy(['objectId' =>$invitation->getObjectId()]);
        $this->view->inviter = $this->session->getPerson();
        $content = $this->view->render('email/invite-family-to-clinic.php');
        $mail->setSubject('Important Taste Invitation');
        $mail->setBodyHtml($content);
        try {
            $mail->send();
        } catch (\Exception $e) {
            echo $mail->getBodyHtmlRendered();
            exit();
        }

        // Send an email to notify clinic manager
        // (only if this is not manager)
        if ($this->session->getClinic()->get('admin')->getObjectId() != $this->session->getUser()->getObjectId()) {
            $mail = new Pimcore_Mail();
            $mail->addTo($invitation->get('email'));
            // Collect invitation with all referred fields. Remember, that for saving purpose we only set up references instead of actual fields
            $this->view->invitation = PRO_InvitationRepository::findOneBy(['objectId' =>$invitation->getObjectId()]);
            $this->view->inviter = $this->session->getPerson();
            $content = $this->view->render('email/notify-family-to-clinic.php');
            $mail->setSubject('Important Taste Invitation');
            $mail->setBodyHtml($content);
            try {
                $mail->send();
            } catch (\Exception $e) {
                echo $mail->getBodyHtmlRendered();
                exit();
            }
        }

        $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
            'type' => 'success',
            'message' => "Successfully sent an invitation"
        ]));
        $this->redirect('/account/edit');
    }


    /**
     * @throws Exception
     */
    public function inviteFamilyMemberAction()
    {
        // Check email is in use
        if ($user = PRO_UserRepository::findOneBy(['email' => $this->getParam('email')], [], true)) {
            if ($this->session->getUser()->getObjectId() == $user->getObjectId()) {
                $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                    'type' => 'error',
                    'message' => 'You can\'t invite yourself'
                ]));
                $this->redirect('/account/edit');
            }
        }

        // ... including invitations
        /*if (PRO_InvitationRepository::findOneBy(['email' => $this->getParam('email'), 'family' => $this->session->getFamily()->getObjectId()])) {
            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'error',
                'message' => 'User with such email is already invited'
            ]));
            $this->redirect('/account/edit');
        }*/

        $invitation = new PRO_InvitationRepository();
        if ($user)
            $invitation->set('registeredUser', new PRO_UserRepository($user->getObjectId()));
        $invitation->set('email', $this->getParam('email'));
        $invitation->set('firstName', $this->getParam('firstName'));
        $invitation->set('lastName', $this->getParam('lastName'));
        $invitation->set('caretaker', true);
        $invitation->set('inviter', new PRO_UserRepository( $this->session->getUser()->getObjectId() ));
        $invitation->set('family', new PRO_FamilyRepository( $this->session->getFamily()->getObjectId() ));

        // Save invitation
        try {
            $invitation->save();
        } catch (BackendlessException $e) {
            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'error',
                'message' => "Couldn't invite with error: {$e->getMessage()}. Please contact administrator."
            ]));
            $this->redirect('/account/edit');
        }

        // Clear cache
        PRO_InvitationRepository::purgeCollectionCache(['family.objectId' => $this->session->getFamily()->getObjectId(), 'caretaker' => true]);

        // Send an email
        $mail = new Pimcore_Mail();
        $mail->addTo($invitation->get('email'));
        // Collect invitation with all referred fields. Remember, that for saving purpose we only set up references instead of actual fields
        $this->view->invitation = PRO_InvitationRepository::findOneBy(['objectId' =>$invitation->getObjectId()]);
        $this->view->inviter = $this->session->getPerson();
        $content = $this->view->render('email/invite-family-member.php');
        $mail->setSubject('Important Taste Invitation');
        $mail->setBodyHtml( $content );
        try {
            $mail->send();
        } catch ( \Exception $e ) {
            echo $mail->getBodyHtmlRendered();
            exit();
        }

        $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
            'type' => 'success',
            'message' => 'Family member invited'
        ]));
        $this->redirect('/account/edit');
    }


    /**
     *
     */
    public function cancelClinicMemberAction()
    {
        if (!$invitation = PRO_InvitationRepository::findOneBy( ['objectId' => $this->getParam('id')] )) {
            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'error',
                'message' => 'Unknown invitation'
            ]));
            $this->redirect('/account/edit');
        }
        if (!$invitation->get('accepted')) {
            // Invitation was pending. Just remove the record
            $invitation->destroy();
            // Clean cache
            PRO_InvitationRepository::purgeCollectionCache(['clinic.objectId' => $this->session->getClinic()->getObjectId(), 'professional' => true]);

            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'success',
                'message' => 'Successfully revoked invitation'
            ]));
            $this->redirect('/account/edit');
        } else {
            $user = PRO_UserRepository::findOneBy(['email' => $invitation->email], [], true);

            $professional = PRO_ProfessionalRepository::findOneBy(['professional.objectId' => $user->getObjectId()], [], true);
            if(!empty($professional)) {
                $professional->destroy();
                PRO_ProfessionalRepository::purgeCollectionCache(['professional' => $professional->getObjectId()]);

            }
            $invitation->destroy();
            // Clean cache
            PRO_InvitationRepository::purgeCollectionCache(['clinic.objectId' => $this->session->getClinic()->getObjectId(), 'professional' => true]);

            // Invitation was accepted. As there is no implementation at this point, just show notice.
            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'error',
                'message' => 'Successfully revoked invitation.'
            ]));
            $this->redirect('/account/edit');
        }
    }


    /**
     *
     */
    public function cancelFamilyMemberAction()
    {
        if (!$invitation = PRO_InvitationRepository::findOneBy( ['objectId' => $this->getParam('id')] )) {
            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'error',
                'message' => 'Unknown invitation'
            ]));
            $this->redirect('/account/edit');
        }

        if (!$invitation->get('accepted')) {
            // Invitation was pending. Just remove the record
            $invitation->destroy();
            // Clean cache
            PRO_InvitationRepository::purgeCollectionCache(['family.objectId' => $this->session->getFamily()->getObjectId(), 'caretaker' => true]);

            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'success',
                'message' => 'Successfully revoked invitation'
            ]));
            $this->redirect('/account/edit');
        } else {
            $children = PRO_CaretakerRepository::getChildren($invitation->inviter->person);
            foreach ($children as $child) {
                $caretaker = new PRO_CaretakerRepository();
                $caretaker->set('caretaker', new PRO_PersonRepository($this->session->getPerson()->getObjectId()));
                $caretaker->set('child', new PRO_PersonRepository($child->getObjectId()));
                $caretaker->set('primary', false);
                try {
                    $caretaker->destroy();
                } catch (BackendlessException $ex) {
                    $this->view->errors[] = "Caretaker couldn't be saved.";
                }
            }
            $invitation->destroy();
            // Clean cache
            PRO_InvitationRepository::purgeCollectionCache(['family.objectId' => $this->session->getFamily()->getObjectId(), 'caretaker' => true]);

            // Invitation was accepted. As there is no implementation at this point, just show notice.
            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'success',
                'message' => 'Successfully revoked invitation'
            ]));
            $this->redirect('/account/edit');
        }
    }


    /**
     * todo update family subscription to not depend in clinic's one anymore
     */
    public function cancelFamilyToClinicAction()
    {
        if (!$invitation = PRO_InvitationRepository::findOneBy( ['objectId' => $this->getParam('id')] )) {
            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'error',
                'message' => 'Unknown invitation'
            ]));
            $this->redirect('/account/edit');
        }

        if (!$invitation->get('accepted')) {
            // Invitation was pending. Just remove the record
            $invitation->destroy();
            // Clean cache
            PRO_InvitationRepository::purgeCollectionCache(['clinic.objectId' => $this->session->getClinic()->getObjectId(), 'caretaker' => true]);
            PRO_InvitationRepository::purgeCollectionCache(['inviter.objectId' => $this->session->getUser()->getObjectId(), 'caretaker' => true]);

            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'success',
                'message' => 'Successfully revoked invitation'
            ]));
            $this->redirect('/account/edit');
        } else {
            // Invitation was accepted.
            // Remove ProfessionalFollow record
            $relation = PRO_ProfessionalFollowRepository::findOneBy(['clinic.objectId' => $this->session->getClinic()->getObjectId(), 'caretaker' => $invitation->get('registeredUser')->getObjectId()]);
            if(!empty($relation)){
                $relation->destroy();
            }
            // Clinics will not part with families too often. Just clear all ProfessionalFollow cache
            PRO_ProfessionalFollowRepository::purgeAllCache();
            // Remove Invitation record
            $invitation->destroy();
            // Clean cache
            PRO_InvitationRepository::purgeCollectionCache(['clinic.objectId' => $this->session->getClinic()->getObjectId(), 'caretaker' => true]);
            PRO_InvitationRepository::purgeCollectionCache(['inviter.objectId' => $this->session->getUser()->getObjectId(), 'caretaker' => true]);

            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'success',
                'message' => 'Successfully removed user from patients'
            ]));
            $this->redirect('/account/edit');
        }
    }


    /**
     *
     */
    public function resendFamilyMemberAction()
    {
        if (!$invitation = PRO_InvitationRepository::findOneBy( ['objectId' => $this->getParam('id')] )) {
            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'error',
                'message' => 'Unknown invitation'
            ]));
            $this->redirect('/account/edit');
        }

        // Send an email
        $mail = new Pimcore_Mail();
        $mail->addTo($invitation->get('email'));
        // Collect invitation with all referred fields. Remember, that for saving purpose we only set up references instead of actual fields
        $this->view->invitation = PRO_InvitationRepository::findOneBy(['objectId' =>$invitation->getObjectId()]);
        $this->view->inviter = $this->session->getPerson();
        $content = $this->view->render('email/invite-family-member.php');
        $mail->setSubject('Important Taste Invitation');
        $mail->setBodyHtml($content);
        try {
            $mail->send();
        } catch (\Exception $e) {
            echo $mail->getBodyHtmlRendered();
            exit();
        }

        $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
            'type' => 'success',
            'message' => 'Family member re-invited'
        ]));
        $this->redirect('/account/edit');
    }


    /**
     *
     */
    public function resendFamilyToClinicAction()
    {
        if (!$invitation = PRO_InvitationRepository::findOneBy( ['objectId' => $this->getParam('id')] )) {
            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'error',
                'message' => 'Unknown invitation'
            ]));
            $this->redirect('/account/edit');
        }

        // Send an email to invited person
        $mail = new Pimcore_Mail();
        $mail->addTo($invitation->get('email'));
        // Collect invitation with all referred fields. Remember, that for saving purpose we only set up references instead of actual fields
        $this->view->invitation = PRO_InvitationRepository::findOneBy(['objectId' =>$invitation->getObjectId()]);
        $this->view->inviter = $this->session->getPerson();
        $content = $this->view->render('email/invite-family-to-clinic.php');
        $mail->setSubject('Important Taste Invitation');
        $mail->setBodyHtml($content);
        try {
            $mail->send();
        } catch (\Exception $e) {
            echo $mail->getBodyHtmlRendered();
            exit();
        }

        $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
            'type' => 'success',
            'message' => "Successfully resent an invitation"
        ]));
        $this->redirect('/account/edit');
    }


    /**
     *
     */
    public function resendClinicMemberAction()
    {
        if (!$invitation = PRO_InvitationRepository::findOneBy( ['objectId' => $this->getParam('id')] )) {
            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'error',
                'message' => 'Unknown invitation'
            ]));
            $this->redirect('/account/edit');
        }

        // Send an email to invited person
        $mail = new Pimcore_Mail();
        $mail->addTo($invitation->get('email'));
        // Collect invitation with all referred fields. Remember, that for saving purpose we only set up references instead of actual fields
        $this->view->invitation = PRO_InvitationRepository::findOneBy(['objectId' =>$invitation->getObjectId()]);
        $this->view->inviter = $this->session->getPerson();
        $content = $this->view->render('email/invite-clinic-member.php');
        $mail->setSubject('Important Taste Invitation');
        $mail->setBodyHtml($content);
        try {
            $mail->send();
        } catch (\Exception $e) {
            echo $mail->getBodyHtmlRendered();
            exit();
        }

        $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
            'type' => 'success',
            'message' => 'Successfully invited person to become clinic employee'
        ]));
        $this->redirect('/account/edit');
    }
}