<?php

/**
 * Class PRO_AccountController
 *
 * Provides possibility to execute actions not related to patients to logged in user:
 * View and edit profile
 * View and edit clinic
 * Manager subscription
 * Invite clinic or family members
 * Invite families to join clinic
 * todo add anchor to redirects
 */
use backendless\Backendless;
use Pimcore\Tool\Session;
use backendless\exception\BackendlessException;
class PRO_AccountController extends PRO_Controller_Protected
{
    /**
     * Render "Manage profile" form
     */
    public function editAction()
    {
        // Reload the subscription from DB, because it might have changed outside of this session (PayPal IPN)
        // $this->session->getSubscription()->fetch();

        // Prepare options for selects
        $this->view->options = new stdClass();
        $this->view->options->country = PRO_CountryRepository::findBy([], ['orderBy' => 'name', 'limit' => 999]);
        $this->view->options->state = PRO_StateRepository::findBy([], ['orderBy' => 'name', 'limit' => 100]);
        $this->view->options->language = PRO_LanguageRepository::findBy([], ['orderBy' => 'name']);

        if ($this->session->getRole() == 'clinic-manager') {
            $this->view->subscriptions = PRO_SubscriptionTypeRepository::findBy(['professional' => true], ['orderBy' => 'feePerMonth']);
            $this->view->patientsNum = PRO_InvitationRepository::countBy(['clinic.objectId' => $this->session->getClinic()->getObjectId(), 'caretaker' => true]);
        }

        // Collect employees
        if ($this->session->getRole() == 'clinic-manager' || $this->session->getRole() == 'clinic-non-manager') {
            $this->view->employees = PRO_InvitationRepository::findBy(['clinic.objectId' => $this->session->getClinic()->getObjectId(), 'professional' => true]);
        }

        // Collect invitations
        if ($this->session->getRole() == 'clinic-manager') {
            $this->view->invitations = PRO_InvitationRepository::findBy(['clinic.objectId' => $this->session->getClinic()->getObjectId(), 'caretaker' => true]);
        } else if ($this->session->getRole() == 'clinic-non-manager') {
            $this->view->invitations = PRO_InvitationRepository::findBy(['inviter.objectId' => $this->session->getUser()->getObjectId(), 'caretaker' => true]);
        } else {
            if ($this->session->getFamily()) {
                $this->view->invitations = PRO_InvitationRepository::findBy(['family.objectId' => $this->session->getFamily()->getObjectId(), 'caretaker' => true]);
            }
        }
    }

    /*
     * ==== FORM PROCESSORS ====
     */

    /**
     * Processes the submit of "My Profile" form
     */
    public function editProfileAction()
    {
        if (!$this->getParam('firstName')) {
            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'error',
                'message' => 'Invalid data to edit'
            ]));
            $this->redirect('/account/edit');
        }
        $session_person = $this->session->getPerson();

        if($session_person) {
            $person = new PRO_PersonRepository($session_person->getObjectId());
        } else {
            $person = new PRO_PersonRepository();
        }
        $person->set('firstName', $this->getParam('firstName'));
        $person->set('lastName', $this->getParam('lastName'));
        if($this->getParam('initials'))
            $person->set('initials', $this->getParam('initials'));
        $person->set('preferredLanguage', new PRO_LanguageRepository($this->getParam('preferredLanguage')));

        if($session_person->get('address')) {
            $address = new PRO_AddressRepository($session_person->get('address')->getObjectId());
        } else {
            $address = new PRO_AddressRepository();
        }
        if ($this->getParam('country')) $address->set('country', new PRO_CountryRepository($this->getParam('country')));
        if ($this->getParam('state')) $address->set('state', new PRO_StateRepository($this->getParam('state')));
        if ($this->getParam('city')) $address->set('city', $this->getParam('city'));
        if ($this->getParam('address')) $address->set('address', $this->getParam('address'));
        if ($this->getParam('postCode')) $address->set('postCode', $this->getParam('postCode'));
        $person->set('address', $address);

        if ($this->getParam('speciality')) {
            $this->session->getProfessional()->set('speciality', $this->getParam('speciality'));
            $this->session->getProfessional()->save();
        }

        try {
            $person->update();
        } catch (BackendlessException $e){
            var_dump($e);die;
        }

        // Professional record is retrieved via query so we need to clean collection cache
        PRO_ProfessionalRepository::purgeCollectionCache(['professional.objectId' => $this->session->getUser()->getObjectId()]);
        Session::get("PRO")->user->person = $person;
        $this->redirect('/account/edit');

    }


    /**
     * todo add more checks for password validity
     * Processed submit of "Change password" form
     */
    public function changePasswordAction()
    {
        if (!$this->getParam('password')) {
            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'error',
                'message' => 'Invalid new password'
            ]));
            $this->redirect('/account/edit');
        }

        try {
            $user = Backendless::$UserService->login($this->session->getUser()->getEmail(), $this->getParam('passwordOld'));
            $user->setPassword($this->getParam('password'));
        } catch (BackendlessException $e) {
            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'error',
                'message' => "Wrong old password"
            ]));
            $this->redirect('/account/edit');
        }

        try {
            Backendless::$UserService->update($user);
        } catch (BackendlessException $e) {
            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'error',
                'message' => "Your profile couldn't be saved with error: {$e->getMessage()}. Please contact administrator."
            ]));
            $this->redirect('/account/edit');
        }


        $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
            'type' => 'success',
            'message' => 'Password updated'
        ]));
        $this->redirect('/account/edit');
    }


    /**
     * @throws Zend_Controller_Router_Exception
     */
    /*
    public function removeFamilyMemberAction()
    {
        if (!$invitation = PRO_InvitationRepository::find($this->getParam('id'), $noCache = true)) {
            throw new Zend_Controller_Router_Exception("Unknown invitation");
        }

        if ($invitation->get('accepted')) {
            throw new Zend_Controller_Router_Exception("Invitation was already accepted");
        }

        if ($invitation->get('accepted')) {
            // Remove the account before removing invitation
            $user = $invitation->get('registeredUser');
            $person = $user->get('person');
            $person->fetch();
            if ($address = $person->get('address')) {
                $address->destroy();
            }
            $caretakers = new \Parse\ParseQuery('Caretaker');
            $caretakers->equalTo('caretaker', $person);
            foreach ($caretakers->find() as $caretaker) {
                $caretaker->destroy();
            }
            $person->destroy();
            $user->destroy();
        }

        $invitation->destroy();
        $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
            'type' => 'success',
            'message' => 'Successfully removed family member',
        ]));

        $this->redirect('/account/edit');
    }
    */


    /**
     * Processes "Clinic" form submit
     */
    public function editClinicAction()
    {
        if (!$this->getParam('name')) {
            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'error',
                'message' => 'Invalid data to edit'
            ]));
            $this->redirect('/account/edit');
        }

        // Prepare vars
        $session_clinic = $this->session->getClinic();

        // clinic
        $clinic = new PRO_ClinicRepository($session_clinic->getObjectId());
        $clinic->set('name', $this->getParam('name'));
        // clinic.address
        $address = new PRO_AddressRepository($session_clinic->get('address')->getObjectId());
        $address->set('country', new PRO_CountryRepository($this->getParam('country')));
        $address->set('state', new PRO_StateRepository($this->getParam('state')));
        $address->set('city', $this->getParam('city'));
        $address->set('address', $this->getParam('address'));
        $address->set('postCode', $this->getParam('postCode'));
        $clinic->set('address', $address);

        // Do the save. It will save all entities via chain so only one call needed.
        try {
            $clinic->update();
        } catch (BackendlessException $e) {
            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'error',
                'message' => "Your clinic couldn't be saved with error: {$e->getMessage()}. Please contact administrator."
            ]));
            $this->redirect('/account/edit');
        }

        // Clear cache
        PRO_ProfessionalRepository::purgeCollectionCache(['professional.objectId' => $this->session->getUser()->getObjectId()]);
        $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
            'type' => 'success',
            'message' => 'Successfully edited clinic'
        ]));
        $this->redirect('/account/edit');
    }

    /*
     * ==== Misc ====
     */

    /**
     * Change subscription type
     * todo Add validation for number of patients
     */
    public function changeSubscriptionAction()
    {
        if (!$newSubscriptionType = PRO_SubscriptionTypeRepository::find($this->getParam('id'))) {
            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'error',
                'message' => 'Unknown subscription'
            ]));
            $this->redirect('/account/edit');
        }

        // Acquire clean version for updates
        $session_subscription = $this->session->getSubscription();
        $subscription = new PRO_SubscriptionRepository($session_subscription->getObjectId());
        $subscription->set('customer', new PRO_UserRepository($this->session->getUser()->getObjectId()));
        $subscription->set('valid', false);
        $subscription->set('startDate', date('m/d/Y H:i O'));
        $subscription->set('endDate', date('m/d/Y H:i O', strtotime('+1 month')));
        $subscription->set('type', new PRO_SubscriptionTypeRepository($this->getParam('id')));

        // Save subscription
        try {
            $subscription->update();
        } catch (BackendlessException $e) {
            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'error',
                'message' => "Couldn't change subscription with error: {$e->getMessage()}. Please contact administrator."
            ]));
            $this->redirect('/account/edit');
        }

        // Clear cache
        PRO_SubscriptionRepository::purgeCollectionCache(['clinic.objectId' => $this->session->getClinic()->getObjectId()]);

        $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
            'type' => 'success',
            'message' => "Changed subscription. Don't forget to pay for your new subscription!"
        ]));
        $this->redirect('/account/edit');
    }


    /**
     * Save child survey data to db
     * todo redirect to previous page rather then fixed
     * @throws Exception
     */
    public function newChildSurveyAction()
    {
        $survey = new PRO_ChildSurveyRepository();
        $survey->set('child', new PRO_PersonRepository($this->getParam('child')));

        // If Skip was clicked, we will simply leave all fields empty
        if ($this->getParam('submit')) {
            $survey->set('followedByProfessional', $this->getParam('followed') ? true : false);
            $data = [
                implode(', ', $this->getParam('speciality', [])),
                $this->getParam('frequency'),
                implode(', ', $this->getParam('symptoms', [])),
            ];
            $survey->set('feedingDifficulties', implode('|', $data));
        }

        // Save survey
        try {
            $survey->save();

            PRO_ChildSurveyRepository::purgeCollectionCache(['child.objectId' => $this->getParam('child')]);

            if ($this->getParam('submit')) {
                $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                    'type' => 'success',
                    'message' => "Thanks for doing this survey!"
                ]));
            }
            $this->redirect('/account/edit');
        } catch (BackendlessException $e) {
            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'error',
                'message' => "Couldn't save survey with error: {$e->getMessage()}. Please contact administrator."
            ]));
            $this->redirect('/account/edit');
        }
    }
}