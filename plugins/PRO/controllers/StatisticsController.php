<?php

/**
 * Class PRO_StatisticsController
 */
class PRO_StatisticsController extends PRO_Controller_Protected
{

    /**
     * @throws Exception
     */
    public function patientAction()
    {
        if ($this->getParam('firstName') && $this->getParam('lastName')) {
            $persons = PRO_PersonRepository::findBy(['firstName' => $this->getParam('firstName'), 'lastName' => $this->getParam('lastName')], array(), true);

            foreach ( $persons as $person) {
                $caretaker = PRO_CaretakerRepository::findBy(['child.objectId' => $person->getObjectId()], [], true);

                foreach ( $caretaker as $caretaker) {
                    $user = PRO_UserRepository::findOneBy(['person.objectId' => $caretaker->get('caretaker')->getObjectId()]);
                    $invitation = PRO_InvitationRepository::findOneBy(['caretaker' => true,
                                                                        'clinic.objectId' => $this->session->getClinic()->getObjectId(),
                                                                        'registeredUser.objectId' => $user->getObjectId(),
                                                                      ], array(), true);

                    if ($invitation) {
                        $this->redirect('/statistics/patient?patient=' . $caretaker->get('child')->getObjectId());
                    }
                }
            }

            $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
                'type' => 'error',
                'message' => 'Such patient doesn\'t exist or you are not allowed to see his/her info.'
            ]));
        }

        // Get a selected patient
        if ($this->getParam('patient')) {
            $this->view->currentPatient = PRO_PersonRepository::findBy(['objectId' => $this->getParam('patient')], [], true);
            $this->view->currentPatient = $this->view->currentPatient[0];
        }

        // If this is family user, collect all children for select
        if ($this->session->getRole() == 'family-manager' || $this->session->getRole() == 'family-non-manager') {
            $this->view->patients = PRO_CaretakerRepository::getChildren($this->session->getPerson());
            if (count($this->view->patients) == 1 && !$this->getParam('patient')) {
                $this->redirect('/statistics/patient?patient=' . $this->view->patients[0]->getObjectId());
            }
        }

        // Get everything from MealSession collection
        if (isset($this->view->currentPatient)) {
            $this->view->meals = PRO_MealSessionRepository::findBy(['person.objectId' => $this->view->currentPatient->getObjectId()],
                                                                        ['orderBy' => 'started', 'order' => 'desc'],
                                                                        true
                                                                    );
            $serializedMealData = $this->serializeMeals($this->view->meals);
            $this->view->mealsSerialized = $serializedMealData;
            $this->view->mealsStatistics = $this->MealsStatistics($serializedMealData);
        }
    }


    protected function serializeMeals($meals)
    {
        $data = [];
        foreach ($meals as $meal) {
            $data[] = [
                'id' => $meal->getObjectId(),
                'started' => $meal->get('started'),
                'finished' => $meal->get('finished'),
                'finishOption' => intval($meal->get('finishOption')),
                'notes' => $meal->get('notes'),
                'thumbnail' => $meal->get('thumbnail') ? (strlen($meal->get('thumbnail')) < 100  ? '//api.backendless.com/2FD42333-7C8B-F088-FF4B-907C3D05F100/v1/files/mealsessionphoto/' . $meal->get('thumbnail') : $meal->get('thumbnail') ) : '/plugins/PRO/static/img/missing-photo.png',
                'photoBefore' => $meal->get('thumbnail') ? (strlen($meal->get('photoBefore')) < 100  ? '//api.backendless.com/2FD42333-7C8B-F088-FF4B-907C3D05F100/v1/files/mealsessionphoto/' . $meal->get('photoBefore') : $meal->get('photoBefore') ) : '/plugins/PRO/static/img/missing-photo.png',
                'photoAfter' => $meal->get('photoAfter') ? (strlen($meal->get('photoAfter')) < 100  ? '//api.backendless.com/2FD42333-7C8B-F088-FF4B-907C3D05F100/v1/files/mealsessionphoto/' . $meal->get('photoAfter') : $meal->get('photoAfter') ) : '/plugins/PRO/static/img/missing-photo.png',
                'mealType' => $meal->get('mealType') ? $meal->get('mealType')->get('stringId') : 'mealtype_drink',
                'mealTypeLocalized' => $this->localize($meal->get('mealType') ? $meal->get('mealType')->get('stringId') : 'mealtype_drink')
            ];
        }

        return json_encode($data);
    }
    function MealsStatistics($mealsSerialized){
        $statistics =  json_decode($mealsSerialized);
        $data_eat = array();
        foreach ($statistics as $statistic){
            $statistic_date = date('d-m-Y',substr($statistic->started, 0, -3));
            if(!isset($data_eat[$statistic_date])){
                $data_eat[$statistic_date] = array($statistic);
            } else {
                $data_eat[$statistic_date][] = $statistic;
            }
        }

        $data_chart = array();
        $data_chart_eat_type = array();
        $data_chart_count = array();
        foreach ($data_eat as $key=>$day){
            $data_chart[$key] = array();
            $day_sum = 0;
            foreach ($day as $food){
                $difference = $food->finished - $food->started;
                $minuteDifference = floor((int)substr($difference, 0, -3) / 60);
                $day_sum =  $day_sum + $minuteDifference;
                $data_chart[$key][$food->finishOption] = (int)$data_chart[$key][$food->finishOption] + $minuteDifference;
                if($food->mealType != 'mealtype_morning_snack' && $food->mealType != 'mealtype_afternoon_snack' && $food->mealType != 'mealtype_evening_snack' && $food->mealType != 'mealtype_evening_snack'){
                    $data_chart_eat_type[$key][$food->mealType]['sum'] = (int)$data_chart_eat_type[$key][$food->mealType]['sum'] + $minuteDifference;
                    $data_chart_eat_type[$key][$food->mealType]['count'] =  $data_chart_eat_type[$key][$food->mealType]['count'] + 1;
                } else {
                    $data_chart_eat_type[$key]['mealtype_snack']['sum'] = (int)$data_chart_eat_type[$key]['mealtype_snack']['sum'] + $minuteDifference;
                    $data_chart_eat_type[$key]['mealtype_snack']['count'] =  $data_chart_eat_type[$key]['mealtype_snack']['count'] + 1;
                }
                $data_chart_count[$key] = $data_chart_count[$key] + 1;
            }

            $finishid_status = array(1,2,3);

            foreach ($finishid_status as $status){
                $percent = $day_sum/100;
                if(isset($data_chart[$key][$status])){
                    $data_chart[$key][$status] =   round(($data_chart[$key][$status] / $percent), 0, PHP_ROUND_HALF_UP);
                } else {
                    $data_chart[$key][$status] = 0;
                }
            }
            ksort($data_chart[$key]);
        }
        $data_none = array();
        $data_tasted = array();
        $data_all_eaten = array();
        foreach ($data_chart as $chart){
            $data_none[] = $chart[1];
            $data_tasted[] = $chart[2];
            $data_all_eaten[]= $chart[3];
        };

        $data_week = array(
                    $this->getDateCalc('-6'),
                    $this->getDateCalc('-5'),
                    $this->getDateCalc('-4'),
                    $this->getDateCalc('-3'),
                    $this->getDateCalc('-2'),
                    $this->getDateCalc('-1'),
                    $this->getDateCalc('0'),
        );
        $data_static_type = array(
            'mealtype_snack' => '',
            'mealtype_dinner' => '',
            'mealtype_lunch' => '',
            'mealtype_breakfast' => '',
            'mealtype_drink' => '',
        );
        $data_chart_count_data = array();
        foreach ($data_week as $day){
            foreach ($data_static_type as $type=>$val){
                if( isset($data_chart_eat_type[$day][$type])){
                    $minutes = round(($data_chart_eat_type[$day][$type]['sum']/$data_chart_eat_type[$day][$type]['count']), 0, PHP_ROUND_HALF_UP);;
                    $data_static_type[$type][] = $minutes > 60 ? 60 : $minutes;
                } else {
                    $data_static_type[$type][] = 0;
                }
            }
            if($data_chart_count[$day]){
                $data_chart_count_data[$day] = $data_chart_count[$day]>10?10:$data_chart_count[$day];
            } else {
                $data_chart_count_data[$day] = 0;
            }
        }
        return array(
            'data_none' => $data_none,
            'data_tasted' => $data_tasted,
            'data_all_eaten' => $data_all_eaten,
            'data_chart' => $data_chart,
            'data_week' => $data_week,
            'data_week_statistic' => $data_static_type,
            'data_chart_count_data' => $data_chart_count_data,
        );

    }

    function getDateCalc($day){
        $now = new DateTime();
        return $now->modify($day.' day')->format('d-m-Y');
    }
    protected function localize($stringId) {
        $data = PRO_LocalizedStringRepository::findBy([
//            'language.isoCode' => 'eng',
            'stringId' => $stringId
        ]);

        return count($data) ? $data[0]->get('localizedString') : $stringId;
    }
}