<?php

/**
 * Class PRO_Paypal_Handler
 *
 * This class needs to provide following possibilities:
 * 1. Start a subscription with monthly charge
 * 2. Check if user is still paying his subscription
 *
 * Unfortunately my last experience with Paypal is quite limited and dates back to 2008 or something so some instructions
 * might be wrong here. Please feel free to send me request for change in behaviour of the class or instructions for
 * additional handling (e.g. if you need to receive request from Paypal or stuff like that)
 *
 * By the way. Subscription - https://www.paypal.com/pdn-recurring
 */
class PRO_Paypal_Handler
{
    /**
     * Configuration constants. Add as many as you need. I will then rework this part to use DI container instead of constants
     */
    const PAYMENT_RECIPIENT = 'emily@hotmail.com';


    /**
     * This method should start a recurring payment from $senderEmail associated account to PAYMENT_RECIPIENT with
     * $monthlyAmount amount charged per month.
     * There is no "pay for 1 year at once" or other stuff. Only subscriptions with monthly charges.
     *
     * Result is an Id or hash that allows to later check status of subscription
     *
     * @param $senderEmail
     * @param $monthlyAmount
     * @return string
     */
    public static function startSubscription($senderEmail, $monthlyAmount)
    {
        $subscriptionId = '';

        return $subscriptionId;
    }


    /**
     * This method checks if the given subscriptions is still active — user does the payments. If it returns false, it
     * would mean user should revoked his access to account
     *
     * @param $subscriptionId
     * @return bool
     */
    public static function checkSubscription($subscriptionId)
    {

        return true;
    }
}