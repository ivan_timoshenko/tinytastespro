<?php

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 1/20/15
 * Time: 3:34 PM
 */
class PRO_Controller_Protected extends PRO_Controller_Public
{
    public function init()
    {
        parent::init();
    }


    public function postDispatch()
    {
        parent::postDispatch();
    }


    public function preDispatch()
    {
        parent::preDispatch();

        if (!$this->session->isActive()) {
            $this->redirect('/user/login');
            //throw new Zend_Controller_Router_Exception("Only logged in users are allowed here");
        }

        // Check that post request contains valid csrf token
        if ($this->getRequest()->isPost()) {
            $session = Pimcore_Tool_Session::get('csrf');
            if (!isset($session->tokens)) {
                $session->tokens = [];
            }
            // Invalidate old tokens
            foreach ($session->tokens as $token => $created) {
                if ($created < (new \DateTime('now'))->modify('-1 hour')) {
                    unset($session->tokens[$token]);
                }
            }
            // Check if provided token is in the list
            if (!isset($session->tokens[$token])) {
//                $this->_helper->getHelper('FlashMessenger')->addMessage(json_encode([
//                    'type' => 'error',
//                    'message' => "Invalid CSRF token"
//                ]));
//                if ($this->session->isActive()) {
//                    $this->redirect($_SERVER['HTTP_REFERER']);
//                }
            }
            // Clear tokens
            $session->tokens = [];
        }

        // Try to find new children (those without Survey so far)
        // Let post request pass — one of them may be survey submit
        if ($this->session->getPerson() && $this->session->getRole() == 'family-manager' && !$this->getRequest()->isPost()) {
            $children = PRO_CaretakerRepository::getChildren($this->session->getPerson());
            foreach ($children as $child) {
                if (!$survey = PRO_ChildSurveyRepository::findOneBy(['child.objectId' => $child->getobjectId()])) {
                    $this->view->newChild = $child;
                    break;
                }
            }
        }
    }


    /**
     * Override getParam to also sanitize data received from user
     * @param string $paramName
     * @param null $default
     * @return mixed|string
     */
    public function getParam($paramName, $default = null)
    {
        $value = parent::getParam($paramName, $default);

        if (is_string($value)) {
            $value = addslashes($value);
        } else if (is_array($value)) {
            // todo filter array too
        }

        return $value;
    }
}