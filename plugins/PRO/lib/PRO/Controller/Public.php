<?php

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 1/20/15
 * Time: 3:34 PM
 */
use backendless\Backendless;
class PRO_Controller_Public extends Website_Controller_Action
{

    /**
     * @var PRO_Session
     */
    protected $session = null;

    /**
     * @var string
     * @enum "professional", "private", "clinic"
     */
    protected $context;


    /**
     *
     */
    public function init()
    {
        parent::init();

        // Prepare Backendless
        require_once dirname(__FILE__) . '/../../backendless/autoload.php';
        //TinyTastes
        Backendless::initApp('2FD42333-7C8B-F088-FF4B-907C3D05F100', '19566C5E-44EB-D602-FFA6-CC2FA6050E00', 'v1');
        //Tiny_Tastes_Production
        //Backendless::initApp('BC5BC1DE-2EA6-6E35-FF5E-1BDCA6A91B00', '34DD4979-D08C-09EA-FF97-350CA2819200', 'v1');

        // todo move to configuration

        $this->view->setScriptPath(
            array_merge(
                $this->view->getScriptPaths(), array(
                    PIMCORE_PLUGINS_PATH . '/PRO/views/layouts/'
                )
            )
        );
        $this->enableLayout();
    }


    /**
     * @throws Exception
     */
    public function preDispatch()
    {
        ini_set('display_errors', '1');
        ob_end_flush();echo "\n";//fix pimcore;
        parent::preDispatch();

        $this->view->messages = [];
        foreach ($this->_helper->getHelper('FlashMessenger')->getMessages() as $message) {
            $this->view->messages[] = json_decode($message);
        }

        $this->session = new PRO_Session();
        $this->session->init();
        $this->view->session = $this->session;
    }
}