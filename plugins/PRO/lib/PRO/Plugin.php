<?php

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 1/20/15
 * Time: 4:05 PM
 */
class PRO_Plugin extends Pimcore_API_Plugin_Abstract
{
    public static function needsReloadAfterInstall()
    {
        return false;
    }


    public static function install()
    {
        $path = self::getInstallPath();

        if (!is_dir($path)) {
            mkdir($path);
        }

        if (self::isInstalled()) {
            return "PRO Plugin successfully installed.";
        } else {
            return "PRO Plugin could not be installed";
        }
    }


    public static function uninstall()
    {
        rmdir(self::getInstallPath());

        if (!self::isInstalled()) {
            return "PRO Plugin successfully uninstalled.";
        } else {
            return "PRO Plugin could not be uninstalled";
        }
    }


    public static function isInstalled()
    {
        return is_dir(self::getInstallPath());
    }


    public static function getTranslationFile($language)
    {

    }


    public static function getInstallPath()
    {
        return PIMCORE_PLUGINS_PATH . "/PRO/install";
    }
}