<?php

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 3/5/15
 * Time: 9:10 PM
 */
use Pimcore\Tool\Session as Session;
use backendless\Backendless;
use \backendless\services\persistence\BackendlessDataQuery;
class PRO_Session
{

    /**
     * @var bool
     */
    protected $active = false;

    /**
     * @var \Parse\ParseUser
     */
    protected $user = null;

    /**
     * @var \PRO_Backendless_Object
     */
    protected $person = null;

    /**
     * @var \Parse\ParseObject
     */
    protected $subscription = null;

    /**
     * @var \Parse\ParseObject
     */
    protected $family = null;

    /**
     * @var \Parse\ParseObject
     */
    protected $professional = null;

    /**
     * @var \Parse\ParseObject
     */
    protected $clinic = null;

    /**
     * @var string
     */
    protected $role = '';


    /**
     * Will automatically collect all data from Parse session + cache + parse.com
     */
    public function init()
    {
        if ($user = Session::get("PRO")->user) {
            // User and person
            $this->setActive(true);
            $this->setUser($user);
            ini_set('display_errors',1);
            if ($user->person) {
                $this->setPerson(PRO_Backendless_Object::_convert($user->person));
            } else {
                $this->setPerson(new PRO_PersonRepository);
            }

            // Professional or Family?
            if ($professional = PRO_ProfessionalRepository::findOneBy(['professional.objectId' => $this->getUser()->getObjectId()])) {
                $this->setProfessional($professional);
                $this->setClinic($professional->get('clinic'));
            } else if ($familyMember = PRO_FamilyMemberRepository::findOneBy(['member.objectId' => $this->getUser()->getObjectId()])) {
                $this->setFamily(PRO_FamilyRepository::findOneBy(['objectId' => $familyMember->get('family')->getObjectId()]));
            } else {
                // User is not professional and not a member of family. Let's create one and add him.
                // todo validate that family with proper admin exists but family member record is missing
                if ($invitation = PRO_InvitationRepository::findOneBy(['registeredUser.objectId' => $this->getUser()->getObjectId()])) {
                    $familyMember = new PRO_FamilyMemberRepository();
                    $familyMember->set('member', new PRO_UserRepository($this->getUser()->getObjectId()));
                    $familyMember->set('family', new PRO_FamilyRepository($invitation->get('family')->getObjectId()));
                    try {
                        $familyMember->save();
                    } catch (Exception $ex) {
                        echo "FamilyMember couldn't be saved.";
                    }
                } else {
                    // familyMember.family
                    $family = new PRO_FamilyRepository();
                    $family->set('admin', new PRO_UserRepository($this->getUser()->getObjectId()));
                    try {
                        $family->save();
                    } catch (Exception $ex) {
                        echo "Family couldn't be saved.";
                    }
                    // familyMember
                    $familyMember = new PRO_FamilyMemberRepository();
                    $familyMember->set('member', new PRO_UserRepository($this->getUser()->getObjectId()));
                    $familyMember->set('family', new PRO_FamilyRepository($family->getObjectId()));
                    try {
                        $familyMember->save();
                    } catch (Exception $ex) {
                        echo "FamilyMember couldn't be saved.";
                    }
                }
                PRO_FamilyMemberRepository::purgeCollectionCache(['member.objectId' => $this->getUser()->getObjectId()]);
                // Reload family with related entities and store in session
                $this->setFamily(PRO_FamilyRepository::findOneBy(['objectId' => $familyMember->get('family')->getObjectId()]));
            }

            // Is it family user invited by clinic?
            if (!$this->getProfessional() && $this->getFamily() && $professionalFollow = PRO_ProfessionalFollowRepository::findOneBy(['caretaker.objectId' => $this->getFamily()->get('admin')])) {
                $this->setClinic($professionalFollow->get('clinic'));
            }

            // Define role in system
            if ($this->getProfessional()) {
                if ($this->getClinic()->get('admin')->getObjectId() == $this->getUser()->getObjectId()) {
                    $this->setRole('clinic-manager');
                } else {
                    $this->setRole('clinic-non-manager');
                }
            } else {
                if ($this->getFamily() && $this->getFamily()->get('admin')->getObjectId() == $this->getUser()->getObjectId()) {
                    $this->setRole('family-manager');
                } else {
                    $this->setRole('family-non-manager');
                }
            }

            // Subscriptions!!!
            if ($this->getProfessional()) {
                // This is a professional and we only need to check clinic's subscription
                if (!$subscription = PRO_SubscriptionRepository::findOneBy(['clinic.objectId' => $this->getClinic()->getObjectId()])) {
                    // No subscription record. Create one.
                    $subscription = new PRO_SubscriptionRepository();
                    // Clinic manager will always be first to login which means we can tell for sure that this is admin
                    $subscription->set('customer', new PRO_UserRepository($user->getObjectId()));
                    $subscription->set('clinic', new PRO_ClinicRepository($professional->get('clinic')->getObjectId()));
                    $subscription->set('valid', false);
                    $subscription->set('type', new PRO_SubscriptionTypeRepository(PRO_SubscriptionTypeRepository::findOneBy(['maxPatients' => 1, 'professional' => true])->getObjectId()));
                    // beta changes. todo remove after beta period ends
                    $subscription->set('valid', true);
                    $subscription->set('startDate', date('m/d/Y H:i O'));
                    $subscription->set('endDate', date('m/d/Y H:i O', strtotime('+1 month')));
                    $subscription->set('type', new PRO_SubscriptionTypeRepository(PRO_SubscriptionTypeRepository::findOneBy(['maxPatients' => 100, 'professional' => true])->getObjectId()));
                    // finish beta changes
                    $subscription->save();
                    // Clear cache
                    PRO_SubscriptionRepository::purgeCollectionCache(['clinic.objectId' => $this->getClinic()->getObjectId()]);
                    // Reload with relations
                    $subscription = PRO_SubscriptionRepository::findOneBy(['clinic.objectId' => $this->getClinic()->getObjectId()]);
                }
            } else {
                if ($this->getFamily() && !$subscription = PRO_SubscriptionRepository::findOneBy(['family.objectId' => $this->getFamily()->getObjectId()])) {
                    // No subscription record. Create one.
                    $subscription = new PRO_SubscriptionRepository();
                    // Family manager will always be first to login which means we can tell for sure that this is manager
                    $subscription->set('customer', new PRO_UserRepository($user->getObjectId()));
                    $subscription->set('family', new PRO_FamilyRepository($this->getFamily()->getObjectId()));
                    $subscription->set('valid', false);
                    $subscription->set('type', new PRO_SubscriptionTypeRepository(PRO_SubscriptionTypeRepository::findOneBy(['maxPatients' => 1, 'professional' => false])->getObjectId()));
                    // Bind subscription to clinic's one if needed
                    //this field is absent in backendless so we need to comment out this block
                    /*if ($this->getClinic()) {
                        // clinic will always have a subscription record so no need to check existence
                        $clinicSubscription = PRO_SubscriptionRepository::findOneBy(['clinic.objectId' => $this->getClinic()->getObjectId()]);
                        $subscription->set('parent', new PRO_SubscriptionRepository($clinicSubscription->getObjectId()));
                    }*/

                    // beta changes. todo remove after beta period ends
                    $subscription->set('valid', true);
                    $subscription->set('startDate', date('m/d/Y H:i O'));
                    $subscription->set('endDate', date('m/d/Y H:i O', strtotime('+1 month')));
                    // finish beta changes
                    $subscription->save();
                    // Clear cache
                    PRO_SubscriptionRepository::purgeCollectionCache(['family.objectId' => $this->getFamily()->getObjectId()]);
                    // Reload with relations
                    $subscription = PRO_SubscriptionRepository::findOneBy(['family.objectId' => $this->getFamily()->getObjectId()]);
                }
            }
            $this->setSubscription($subscription);
        }
    }


    /**
     * Returns true if subscription is active and visitor is allowed to use the system
     * todo add endDate validation
     * @return bool
     * @throws Exception
     */
    public function isSubscriptionValid()
    {
        if ($this->getSubscription()->get('clinic')) {
            // Check number clinic patients against subscription max patients to find out if it is valid
            if ($this->getSubscription()->get('valid') && PRO_InvitationRepository::countBy(['clinic' => $this->getClinic(), 'caretaker' => true]) >= $this->getSubscription()->get('type')->get('maxPatients')) {
                return true;
            } else {
                return false;
            }
        } else if ($this->getSubscription()->get('valid')) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }


    /**
     * @param boolean $active
     * @return PRO_Session
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }


    /**
     * @return PRO_Backendless_Object
     */
    public function getUser()
    {
        return $this->user;
    }


    /**
     * @param PRO_Backendless_Object $user
     * @return PRO_Session
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }


    /**
     * @return PRO_Backendless_Object
     */
    public function getPerson()
    {
        return $this->person;
    }


    /**
     * @param PRO_Backendless_Object $person
     * @return PRO_Session
     */
    public function setPerson($person)
    {
        $this->person = $person;
        return $this;
    }


    /**
     * @return PRO_Backendless_Object
     */
    public function getSubscription()
    {
        return $this->subscription;
    }


    /**
     * @param PRO_Backendless_Object $subscription
     * @return PRO_Session
     */
    public function setSubscription($subscription)
    {
        $this->subscription = $subscription;
        return $this;
    }


    /**
     * @return PRO_Backendless_Object
     */
    public function getFamily()
    {
        return $this->family;
    }


    /**
     * @param PRO_Backendless_Object $family
     * @return PRO_Session
     */
    public function setFamily($family)
    {
        $this->family = $family;
        return $this;
    }


    /**
     * @return PRO_Backendless_Object
     */
    public function getProfessional()
    {
        return $this->professional;
    }


    /**
     * @param PRO_Backendless_Object $professional
     * @return PRO_Session
     */
    public function setProfessional($professional)
    {
        $this->professional = $professional;
        return $this;
    }


    /**
     * @return PRO_Backendless_Object
     */
    public function getClinic()
    {
        return $this->clinic;
    }


    /**
     * @param PRO_Backendless_Object $clinic
     * @return PRO_Session
     */
    public function setClinic($clinic)
    {
        $this->clinic = $clinic;
        return $this;
    }


    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }


    /**
     * @param string $role
     * @return PRO_Session
     */
    public function setRole($role)
    {
        $this->role = $role;
        return $this;
    }


}