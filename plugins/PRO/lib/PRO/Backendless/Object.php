<?php
use backendless\model\Data;
use \backendless\Backendless;
class PRO_Backendless_Object extends PRO_Backendless_Repository {
    public function __construct($data = []) {
        if (is_string($data)) {
            $data = ['objectId' => $data];
        }
        if (!isset($data['___class']) && isset(static::$className)) {
            $data['___class'] = static::$className;
        }
        foreach((array) $data as $key => $value) {
            $this->$key = $value;
        }
        if (!isset($this->objectId)) $this->objectId = null;
    }

    public function set($name, $value) {
        $this->$name = $value;
    }

    public function get($name) {
        if (!isset($this->$name)) return null;
        if (is_object($this->$name) || is_array($this->$name)) return self::_new($this->$name);
        return $this->$name;
    }

    public function has($string){
        return isset($this->$string);
    }

    public function __toString() {
        return ''.$this->objectId;
    }

    public static function _convert($data) {
        if (is_array($data)) {
            if (isset($data[0]) || empty($data)) {
                foreach ($data as $i => $value) {
                    $data[$i] = self::_new($value);
                }
                return $data;
            } else {
                return self::_new($data);
            }
        } elseif (is_object($data)) {
            return self::_new($data);
        }
    }

    public static function _new($data) {
        if (is_object($data)) {
            if (is_a($data, 'PRO_Backendless_Object')) {
                return $data;
            }
            if (is_a( $data, "stdClass" )) {
                $data = (array) $data;
            }
        }
        $class = 'PRO_Backendless_Object';
        if( isset( $data[ 'table-name' ] ) ) {
            $class = 'PRO_' . $data[ 'table-name' ] . 'Repository';
        }  elseif( isset( $data[ '___class' ] ) ) {
            $class = 'PRO_' . $data[ '___class' ] . 'Repository';
        } else {
            //throw new Exception( 'Missing "table-name" in data multi array' );
        }
        return (class_exists($class)) ? new $class($data) : new self($data);
    }
}