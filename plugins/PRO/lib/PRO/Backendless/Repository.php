<?php

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 2/1/15
 * Time: 10:54 PM
 * todo add cleaning from pimcore cache fields to let controllers passed cached objects to parse.com
 */
use backendless\Backendless;
use backendless\model\Data;
use backendless\exception\BackendlessException;
use backendless\services\persistence\BackendlessDataQuery;
abstract class PRO_Backendless_Repository extends Data
{
    /**
     * @var string
     */
    protected static $className = "";

    /**
     * @var array
     */
    protected static $cacheTags = [];

    /**
     * @var int
     */
    protected static $cacheLifetime = 0;

    /**
     * @var array
     */
    protected static $includedKeys = [];


    /**
     *
     */
    public static function purgeAllCache()
    {
        Pimcore_Model_Cache::clearTags(static::$cacheTags);
    }


    /**
     * @param array $tags
     */
    protected static function purgeCacheByTags(array $tags)
    {
        Pimcore_Model_Cache::clearTags($tags);
    }


    /**
     * @param PRO_Backendless_Object[] $items
     */
    public static function purgeItemsCache(array $items)
    {
        foreach ($items as $item) {
            Pimcore_Model_Cache::remove(static::buildCacheKey($item));
        }
    }


    /**
     * @param array $params
     * @param array $options
     * @return mixed
     */
    public static function purgeCollectionCache(array $params = [], array $options = [])
    {
        Pimcore_Model_Cache::remove(static::buildCacheKey($params, $options));
        // countBy add's it's own option to have different key, so we need to clear it too
        Pimcore_Model_Cache::remove(static::buildCacheKey($params, ['count' => 1]));
    }


    /**
     * @param $id
     * @param bool $noCache
     * @return array|mixed
     * @throws backendless\exception\BackendlessException
     */
    public static function find($id, $noCache = false)
    {
        if ($noCache || !$data = static::loadFromCache(static::buildCacheKey($id))) {

            $data = PRO_Backendless_Object::_convert(Backendless::$Persistence->of(static::getClassName())->findById($id));
            if(!$noCache)
                static::cache(static::buildCacheKey($id), serialize($data));
            return $data;
        }
        return unserialize($data);
    }


    /**
     * @param array $params
     * @param array $options
     * @return mixed
     */
    public static function findBy(array $params = [], array $options = [], $noCache = false)
    {
        if ($noCache || !$result = static::loadFromCache(static::buildCacheKey($params, $options))) {
            $query = new BackendlessDataQuery();

            $where = [];
            foreach ($params as $param => $value) {
                if (is_object($value)) {
                    $value = method_exists($value, 'getObjectId') ? $value->getObjectId() : (isset($value->objectId) ? $value->objectId : '*');
                }
                $where[] = "$param = '$value'";
            }
            if ($where) $query->setWhereClause(implode(' AND ', $where));
            if (isset($options['orderBy'])) $query->setSortBy(is_array($options['orderBy']) ? $options['orderBy'] : [$options['orderBy']]);
            if (isset($options['limit'])) $query->setPageSize($options['limit'] > 100 ? 100 : $options['limit'] );

//          var_dump($where);//debug_print_backtrace();
            $result = [];
            try {
                // max lim in backendless = 100
                if(isset($options['limit'])){
                    $query->setPageSize($options['limit'] > 100 ? 100 : $options['limit'] );
                    for ($i = 0; $i <= $options['limit']; $i += 100) {
                        $query->setOffset($i);
                        $response = Backendless::$Persistence->of(static::getClassName())->find($query)->getAsObjects();
                        $result = array_merge($result, $response);
                        if(count($response) < 100) break;
                    }
                } else {
                    $result = Backendless::$Persistence->of(static::getClassName())->find($query)->getAsObjects();
                }
                $result = PRO_Backendless_Object::_convert($result);
            } catch(BackendlessException $e) {
                var_dump($e);die;
            }
            $result = serialize($result);
            static::cache(static::buildCacheKey($params, $options), $result);
        }
        return unserialize($result);
    }


    public static function countBy(array $params = [])
    {
        //var_dump(get_defined_vars());die;
        if (!$data = static::loadFromCache(static::buildCacheKey($params, ['count' => 1]))) {
            $query = new BackendlessDataQuery();
            $query->setPageSize(1);
            $where = [];
            foreach ($params as $param => $value) {
                if (is_object($value)) {
                    $value = method_exists($value, 'getObjectId') ? $value->getObjectId() : (isset($value->objectId) ? $value->objectId : '*');
                }
                $where[] = "$param = '$value'";
            }
            if ($where) $query->setWhereClause(implode(' AND ', $where));
            try {
                $data = Backendless::$Persistence->of(static::getClassName())->find($query)->totalObjectsCount();
            } catch (BackendlessException $e) {
                var_dump($e);die;
            }
            static::cache(static::buildCacheKey($params, ['count' => 1]), $data);
        }

        return $data;
    }

    /**
     * @param array $params
     * @param array $options
     * @return mixed
     */
    public static function findOneBy(array $params = [], array $options = [], $noCache = false)
    {
        $items = static::findBy($params, $options, $noCache);
        if (count($items)) {
            return array_pop($items);
        } else {
            return null;
        }
    }


    /**
     * @param $key
     * @return mixed
     */
    protected static function loadFromCache($key)
    {
        //self::purgeAllCache();
        return Pimcore_Model_Cache::load($key);
    }


    /**
     * @param $key
     * @param $data
     * @param array $supplementaryTags
     */
    protected static function cache($key, $data, array $supplementaryTags = [])
    {
        Pimcore_Model_Cache::save($data, $key, array_merge( static::$cacheTags, $supplementaryTags), static::$cacheLifetime);
    }


    /**
     * @param $params
     * @param array $options
     * @return string
     */
    protected static function buildCacheKey($params, $options = [])
    {
        if (is_string($params)) {
            $return = 'backend_' . static::$className . '_' . $params;
        } else {
            $return = 'backend_' . static::$className . '_' . md5(self::serializeParams($params)) . (count($options) ? '_' . md5(serialize($options)) : '');
        }
        return $return;
    }


    /**
     * When serializing, we want to use only objectId when PRO_Backendless_Object are passed for check
     * this way we make sure that objects will always generate same key no matter how they were acquired
     * e.g. Person retrieved from User might differ from Person which was separately queried (more fields etc)
     *
     * todo merge method content to buildCacheKey
     * @param $params
     * @return string
     */
    protected function serializeParams($params)
    {
        $serialized = [];
        foreach ($params as $key => $value) {
            if (method_exists($value, 'getObjectId')) {
                $serialized[$key] = $value->getObjectId();
            } else {
                $serialized[$key] = $value;
            }
        }

        return serialize($serialized);
    }


    /**
     * @return string
     */
    public static function getClassName()
    {
        return static::$className;
    }


    /**
     * @return array
     */
    public static function getCacheTags()
    {
        return static::$cacheTags;
    }


    /**
     * @return int
     */
    public static function getCacheLifetime()
    {
        return static::$cacheLifetime;
    }

    public function update() {
        if (isset($this->___class)) {
            $class = $this->___class;
        } else {
            $this->___class = $class = static::$className;
        }

        $saved = Backendless::$Persistence->of($class)->save((array)$this);

        foreach((array) $saved as $key => $value) {
            $this->$key = $value;
        }
    }

    public function save() {
        $this->update();
    }

    public function destroy() {
        if (isset($this->___class)) {
            $class = $this->___class;
        } else {
            $this->___class = $class = static::$className;
        }
        if(!$class){
            $class = $this->table_name;
        }
        $obj_Id = $this->getObjectId();
        if(!empty($obj_Id) && $class){
            $a = new stdClass();
            $a->table_name = $class;
            $a->objectId = $this->getObjectId();
            Backendless::$Persistence->of($class)->remove($a);
        } else {
            var_dump('can\'t find table class in this object', $this);die;
        }
    }

}