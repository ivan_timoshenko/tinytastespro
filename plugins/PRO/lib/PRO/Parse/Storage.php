<?php

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 1/20/15
 * Time: 5:54 PM
 */
class PRO_Parse_Storage implements \Parse\ParseStorageInterface
{

    /**
     * Returns an instance of Zend_Session_Container
     *
     * @return stdClass
     */
    protected function getSession()
    {
        return \Pimcore\Tool\Session::get("PRO");
    }


    /**
     * Sets a key-value pair in storage.
     *
     * @param string $key The key to set
     * @param mixed $value The value to set
     *
     * @return null
     */
    public function set($key, $value)
    {
        $this->getSession()->{$key} = $value;
    }


    /**
     * Remove a key from storage.
     *
     * @param string $key The key to remove.
     *
     * @return null
     */
    public function remove($key)
    {
        $this->getSession()->{$key} = null;
    }


    /**
     * Gets the value for a key from storage.
     *
     * @param string $key The key to get the value for
     *
     * @return mixed
     */
    public function get($key)
    {
        return $this->getSession()->{$key};
    }


    /**
     * Clear all the values in storage.
     *
     * @return null
     */
    public function clear()
    {
        $session = $this->getSession();
        $session = null;
    }


    /**
     * Save the data, if necessary.  This would be a no-op when using the
     * $_SESSION implementation, but could be used for saving to file or
     * database as an action instead of on every set.
     *
     * @return null
     */
    public function save()
    {
        // TODO: Implement save() method.
    }


    /**
     * Get all keys in storage.
     *
     * @return array
     */
    public function getKeys()
    {
        return array_keys($this->getSession());
    }


    /**
     * Get all key-value pairs from storage.
     *
     * @return array
     */
    public function getAll()
    {
        return $this->getSession();
    }

}