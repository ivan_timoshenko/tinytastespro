<?php

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 2/15/15
 * Time: 4:41 PM
 */
class PRO_ClinicRepository extends PRO_Backendless_Object
{
    /**
     * @var string
     */
    protected static $className = "Clinic";

    /**
     * @var array
     */
    protected static $cacheTags = ['clinic'];

    /**
     * @var int
     */
    protected static $cacheLifetime = 86400; // One day

    /**
     * @var array
     */
    protected static $includedKeys = [
        'address',
        'admin',
    ];
}