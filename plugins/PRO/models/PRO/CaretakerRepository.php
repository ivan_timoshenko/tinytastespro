<?php

/**
 * Class PRO_CaretakerRepository
 */
class PRO_CaretakerRepository extends PRO_Backendless_Object
{
    /**
     * @var string
     */
    protected static $className = "Caretaker";

    /**
     * @var array
     */
    protected static $cacheTags = ['caretaker'];

    /**
     * @var int
     */
    protected static $cacheLifetime = 86400; // One day

    /**
     * @var array
     */
    protected static $includedKeys = [
        'child',
    ];


    /**
     * This method will strip Caretaker related fields leaving only array of Person
     * @param PRO_Backendless_Object $caretaker
     * @return mixed
     */
    public static function getChildren($caretaker)
    {
        if(is_array($caretaker)){
            $relations = static::findBy(['caretaker.objectId' => $caretaker['objectId']], [], true);
        } else {
            $relations = static::findBy(['caretaker.objectId' => $caretaker->objectId], [], true);
        }
        $children = [];
        foreach ($relations as $relation) {
            $children[] = $relation->get('child');
        }

        return $children;
    }
}