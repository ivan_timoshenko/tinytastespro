<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 3/2/15
 * Time: 2:36 PM
 */

class PRO_FamilyMemberRepository extends PRO_Backendless_Object
{
    /**
     * @var string
     */
    protected static $className = "FamilyMember";

    /**
     * @var array
     */
    protected static $cacheTags = ['familyMember'];

    /**
     * @var int
     */
    protected static $cacheLifetime = 86400; // One day

    /**
     * @var array
     */
    protected static $includedKeys = [
        'member',
        'family',
        'family.admin',
    ];
}