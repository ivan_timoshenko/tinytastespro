<?php

/**
 * Class PRO_ProfessionalRepository
 */
class PRO_ProfessionalRepository extends PRO_Backendless_Object
{
    /**
     * @var string
     */
    protected static $className = "Professional";

    /**
     * @var array
     */
    protected static $cacheTags = ['professional'];

    /**
     * @var int
     */
    protected static $cacheLifetime = 86400; // One day

    /**
     * @var array
     */
    protected static $includedKeys = [
        'clinic',
        'clinic.address',
        'professional',
        'professional.person'
    ];
}