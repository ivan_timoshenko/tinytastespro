<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 4/5/15
 * Time: 11:46 PM
 */

class PRO_ChildSurveyRepository extends PRO_Backendless_Object
{
    /**
     * @var string
     */
    protected static $className = "ChildSurvey";

    /**
     * @var array
     */
    protected static $cacheTags = ['survey'];

    /**
     * @var int
     */
    protected static $cacheLifetime = 31536000; // One year

    /**
     * @var array
     */
    protected static $includedKeys = [
    ];
}