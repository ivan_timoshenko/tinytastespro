<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 3/2/15
 * Time: 2:38 PM
 */

class PRO_FamilyRepository extends PRO_Backendless_Object
{
    /**
     * @var string
     */
    protected static $className = "Family";

    /**
     * @var array
     */
    protected static $cacheTags = ['family'];

    /**
     * @var int
     */
    protected static $cacheLifetime = 86400; // One day

    /**
     * @var array
     */
    protected static $includedKeys = [
        'admin',
    ];
}