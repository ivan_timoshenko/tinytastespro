<?php

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 2/28/15
 * Time: 5:28 PM
 */
class PRO_InvitationRepository extends PRO_Backendless_Object
{
    /**
     * @var string
     */
    protected static $className = "Invitation";

    /**
     * @var array
     */
    protected static $cacheTags = ['invitation'];

    /**
     * @var int
     */
    protected static $cacheLifetime = 86400; // One day

    /**
     * @var array
     */
    protected static $includedKeys = [
        'inviter',
        'inviter.person',
        'registeredUser',
        'registeredUser.person',
        'clinic',
    ];
}