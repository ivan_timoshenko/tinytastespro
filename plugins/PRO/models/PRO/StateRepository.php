<?php

/**
 * Class StateRepository
 */
class PRO_StateRepository extends PRO_Backendless_Object
{
    /**
     * @var string
     */
    protected static $className = "State";

    /**
     * @var array
     */
    protected static $cacheTags = ['state'];

    /**
     * @var int
     */
    protected static $cacheLifetime = 31536000; // One year

    /**
     * @var array
     */
    protected static $includedKeys = [];
}