<?php

/**
 * Class PRO_AddressRepository
 */
class PRO_AddressRepository extends PRO_Backendless_Object
{
    /**
     * @var string
     */
    protected static $className = "Address";

    /**
     * @var array
     */
    protected static $cacheTags = ['address'];

    /**
     * @var int
     */
    protected static $cacheLifetime = 86400; // One day

    /**
     * @var array
     */
    protected static $includedKeys = [
        'country',
        'state'
    ];
}