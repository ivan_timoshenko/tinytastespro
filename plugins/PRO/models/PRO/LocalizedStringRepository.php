<?php

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 2/5/15
 * Time: 5:45 PM
 */
class PRO_LocalizedStringRepository extends PRO_Backendless_Object
{
    /**
     * @var string
     */
    protected static $className = "LocalizedString";

    /**
     * @var array
     */
    protected static $cacheTags = ['localization'];

    /**
     * @var int
     */
    protected static $cacheLifetime = 604800; // One week (todo consider caching it for eternity)

    /**
     * @var array
     */
    protected static $includedKeys = [
        'language'
    ];
}