<?php

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 2/2/15
 * Time: 12:00 AM
 */
class PRO_MealSessionRepository extends PRO_Backendless_Object
{
    /**
     * @var string
     */
    protected static $className = "MealSession";

    /**
     * @var array
     */
    protected static $cacheTags = ['meal_session'];

    /**
     * @var int
     */
    protected static $cacheLifetime = 86400; // One day

    /**
     * @var array
     */
    protected static $includedKeys = [
        'mealType',
        'mealType.name',
        'person',
    ];
}