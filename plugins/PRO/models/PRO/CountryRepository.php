<?php

/**
 * Class CountryRepository
 */
class PRO_CountryRepository extends PRO_Backendless_Object
{
    /**
     * @var string
     */
    protected static $className = "Country";

    /**
     * @var array
     */
    protected static $cacheTags = ['country'];

    /**
     * @var int
     */
    protected static $cacheLifetime = 31536000; // One year

    /**
     * @var array
     */
    protected static $includedKeys = [];
}