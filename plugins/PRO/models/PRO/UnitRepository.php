<?php

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 2/1/15
 * Time: 11:24 PM
 */
class PRO_UnitRepository extends PRO_Backendless_Object
{
    /**
     * @var string
     */
    protected static $className = "Unit";

    /**
     * @var array
     */
    protected static $cacheTags = ['unit'];

    /**
     * @var int
     */
    protected static $cacheLifetime = 31536000; // One day

    /**
     * @var array
     */
    protected static $includedKeys = [];

}