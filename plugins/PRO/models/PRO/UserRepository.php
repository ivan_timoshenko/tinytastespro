<?php

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 2/28/15
 * Time: 5:37 PM
 */
class PRO_UserRepository extends PRO_Backendless_Object
{
    /**
     * @var string
     */
    protected static $className = "Users";

    /**
     * @var array
     */
    protected static $cacheTags = ['users'];

    /**
     * @var int
     */
    protected static $cacheLifetime = 86400; // One day

    /**
     * @var array
     */
    protected static $includedKeys = [
        'person'
    ];
}