<?php

/**
 * Class LanguageRepository
 */
class PRO_LanguageRepository extends PRO_Backendless_Object
{
    /**
     * @var string
     */
    protected static $className = "Language";

    /**
     * @var array
     */
    protected static $cacheTags = ['language'];

    /**
     * @var int
     */
    protected static $cacheLifetime = 31536000; // One year

    /**
     * @var array
     */
    protected static $includedKeys = [];
}