<?php

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 3/13/15
 * Time: 7:43 PM
 */
class PRO_ProfessionalFollowRepository extends PRO_Backendless_Object
{
    /**
     * @var string
     */
    protected static $className = "ProfessionalFollow";

    /**
     * @var array
     */
    protected static $cacheTags = ['professionalFollow'];

    /**
     * @var int
     */
    protected static $cacheLifetime = 86400; // One day

    /**
     * @var array
     */
    protected static $includedKeys = [
        'clinic',
        'family',
        'family.admin',
        'family.address',
    ];
}