<?php

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 2/1/15
 * Time: 11:47 PM
 */
class PRO_PersonRepository extends PRO_Backendless_Object
{
    /**
     * @var string
     */
    protected static $className = "Person";

    /**
     * @var array
     */
    protected static $cacheTags = ['person'];

    /**
     * @var int
     */
    protected static $cacheLifetime = 86400; // One day

    /**
     * @var array
     */
    protected static $includedKeys = [
        'address',
        'address.country',
        'address.state',
        'preferredLanguage',
    ];
}