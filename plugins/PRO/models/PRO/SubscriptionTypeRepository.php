<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 2/18/15
 * Time: 11:45 PM
 */

class PRO_SubscriptionTypeRepository extends PRO_Backendless_Object
{
    /**
     * @var string
     */
    protected static $className = "SubscriptionType";

    /**
     * @var array
     */
    protected static $cacheTags = ['subscriptionType'];

    /**
     * @var int
     */
    protected static $cacheLifetime = 31536000; // One year

    /**
     * @var array
     */
    protected static $includedKeys = [
    ];
}