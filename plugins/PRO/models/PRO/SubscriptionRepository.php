<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 2/18/15
 * Time: 11:45 PM
 */

class PRO_SubscriptionRepository extends PRO_Backendless_Object
{
    /**
     * @var string
     */
    protected static $className = "Subscription";

    /**
     * @var array
     */
    protected static $cacheTags = ['subscription'];

    /**
     * @var int
     */
    protected static $cacheLifetime = 86400; // One year

    /**
     * @var array
     */
    protected static $includedKeys = [
        'type'
    ];
	
	
	public static function findUserActiveSubscription ( $userId ) {
		$query = new \Parse\ParseQuery(static::getParseClassName());
		$query->equalTo("customer", $userId);
		$query->equalTo("valid", true);
		$query->greaterThan("endDate", date('m/d/Y H:i O'));
		
		return self::findOneBy(['objectId' => $query->find()[0]->getObjectId()]);
	}
}