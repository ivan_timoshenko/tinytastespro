$(document).ready(function () {
    $('.tabbed-content').tabs();

    $('.flash-message .close').on('click', function () {
        $(this).closest('.flash-message').remove();
    });

    $('.modal').on('click', function (event) {
        var dialog = $($(this).attr('href')).dialog({
            modal: true,
            buttons: {
                Cancel: function () {
                    dialog.dialog("close");
                }
            },
            draggable: false,
            resizable: false,
            width: 600
        });

        event.preventDefault();
    });

    // Recalculate sub-menu width for proper alignment
    $('.main-part .main-menu').width($('.main-part .main-menu li').length * 260);
});
