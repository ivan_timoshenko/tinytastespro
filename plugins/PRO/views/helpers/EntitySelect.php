<?php

/**
 * Class PRO_View_Helper_EntitySelect
 */
class PRO_View_Helper_EntitySelect extends Zend_View_Helper_Abstract
{


    /**
     * @param $name
     * @param array $values
     * @param $titleField
     * @param $selected
     * @param array $options
     * @todo add possibility to override isDefault field name
     */
    public function entitySelect($name, array $values, $titleField, $selected, $options = [])
    {
        ?>
        <select name="<?= $name; ?>" id="<?= $name; ?>">
            <?php foreach ($values as $option): ?>
                <option
                    value="<?= $option->getObjectId(); ?>"
                    <?= ($option->getObjectId() == $selected) || ($selected === null && $option->has('isDefault') && $option->get('isDefault')) ? 'selected' : ''; ?>>
                    <?= $option->get($titleField); ?>
                </option>
            <?php endforeach; ?>
        </select>
    <?php
    }
}