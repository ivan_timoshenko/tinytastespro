<?php

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 3/12/15
 * Time: 12:29 PM
 */
class PRO_View_Helper_CSRF extends Zend_View_Helper_Abstract
{
    public function CSRF()
    {
        $session = Pimcore_Tool_Session::get('csrf');
        if (!isset($session->tokens)) {
            $session->tokens = [];
        }
        $token = md5(uniqid());
        $session->tokens[$token] = new \DateTime('now');

        echo '<input type="hidden" name="csrf" value="' . $token . '"/>';
    }
}