<?php

/**
 * Class PRO_View_Helper_Localize
 * todo add proper filtering by language instead of picking first one.
 */
class PRO_View_Helper_Localize extends Zend_View_Helper_Abstract
{
    /**
     * @var string
     */
    protected static $currentLanguageCode = 'eng';


    /**
     * @param $stringId
     * @return mixed
     */
    public function localize($stringId)
    {
        $data = PRO_LocalizedStringRepository::findBy([
//            'language.isoCode' => 'eng',
            'stringId' => $stringId
        ]);

        return count($data) ? $data[0]->get('localizedString') : $stringId;
    }
}