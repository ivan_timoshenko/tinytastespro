<!doctype html>
<html lang="en">
<head>
    <link href="/plugins/PRO/static/css/no-theme/jquery-ui-1.10.4.custom.min.css" media="screen, projection" rel="stylesheet" type="text/css"/>
    <link href="/plugins/PRO/static/css/screen.css" media="screen, projection" rel="stylesheet" type="text/css"/>
    <link href="/plugins/PRO/static/css/print.css" media="print" rel="stylesheet" type="text/css"/>
    <!--[if IE]>
    <link href="/plugins/PRO/static/css/ie.css" media="screen, projection" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <script type="text/javascript" src="/plugins/PRO/static/js/jquery.js"></script>
    <script type="text/javascript" src="/plugins/PRO/static/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/plugins/PRO/static/js/main.js"></script>
    <!--[if lt IE 9]>
    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
    <![endif]-->
</head>
<body>

<div class="container">
    <div class="page-header">
        <a class="logo" href="/"><img src="/plugins/PRO/static/img/logo.png" alt=""/></a>

        <nav class="help" style="float: right; margin-right: 100px;">
            <a href="/for-families/faq-families">?</a>
        </nav>
    </div>
</div>

<div class="container">
    <div class="main-part">

        <p>&nbsp;</p>

        <?= $this->layout()->content; ?>
    </div>
</div>

<?= $this->placeholder('scripts') ?>
</body>
</html>