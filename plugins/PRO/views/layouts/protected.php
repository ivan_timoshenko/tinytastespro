<!doctype html>
<html lang="en" style="height: 100%">
<head>
    <link href="/plugins/PRO/static/css/no-theme/jquery-ui-1.10.4.custom.min.css" media="screen, projection" rel="stylesheet" type="text/css"/>
    <link href="/plugins/PRO/static/css/screen.css" media="screen, projection" rel="stylesheet" type="text/css"/>
    <link href="/plugins/PRO/static/css/print.css" media="print" rel="stylesheet" type="text/css"/>
    <!--[if IE]>
    <link href="/plugins/PRO/static/css/ie.css" media="screen, projection" rel="stylesheet" type="text/css"/>
    <![endif]-->

    <script type="text/javascript" src="/plugins/PRO/static/js/jquery.js"></script>
    <script type="text/javascript" src="/plugins/PRO/static/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js"></script>
    <script type="text/javascript" src="/plugins/PRO/static/js/main.js"></script>
    <script type="text/javascript" src="/plugins/PRO/static/js/moment.js"></script>
    <script type="text/javascript" src="/plugins/PRO/static/js/Chart.js"></script>
    <!--[if lt IE 9]>
    <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
    <![endif]-->
</head>
<body style="display: flex; flex-direction: column; height: 100%">

<div class="container">
    <div class="page-header">
        <a class="logo" href="/"><img src="/plugins/PRO/static/img/logo.png" alt=""/></a>

        <nav class="help">
            <?php if ($this->session->getRole() == 'clinic-manager' || $this->session->getRole() == 'clinic-non-manager'): ?>
                <a target="_blank" href="/for-professionals/faq-professionals">?</a>
            <?php else: ?>
                <a target="_blank" href="/for-families/faq-families">?</a>
            <?php endif; ?>
        </nav>

        <nav class="main-menu">
            <ul>
                <li>
                    <a href="<?= $this->url(['controller' => 'Statistics', 'action' => 'patient'], 'PRO', true); ?>"
                       class="<?= $this->getParam('controller') == 'statistics' ? 'active' : ''; ?>">
                        <?php if ($this->session->getRole() == 'clinic-manager' || $this->session->getRole() == 'clinic-non-manager'): ?>
                            Your patients
                        <?php else: ?>
                            Your children
                        <?php endif; ?>
                    </a>
                </li>
                <li>
                    <a href="<?= $this->url(['controller' => 'Account', 'action' => 'edit'], 'PRO', true); ?>"
                       class="<?= $this->getParam('controller') == 'account' ? 'active' : ''; ?>">Manage account</a>
                </li>
            </ul>
        </nav>

        <div class="status">
            <div class="wrapper">
                <div class="current-user">
                    <span class="label">Current user:</span>
                    <?php if ($this->session->getRole() == 'clinic-manager' || $this->session->getRole() == 'clinic-non-manager'): ?>
                        <a class="user" href="#"><?= $this->session->getClinic()->get('name'); ?></a>&nbsp;
                    <?php endif; ?>
                    <a class="user" href="#"><?= $this->session->getPerson()->get('initials'); ?> <?= $this->session->getPerson()->get('firstName'); ?> <?= $this->session->getPerson()->get('lastName'); ?></a>
                </div>

                <a class="logout" href="<?= $this->url(['controller' => 'User', 'action' => 'logout'], 'PRO', true); ?>">Logout</a>
            </div>
        </div>
    </div>
</div>

<div class="container" style="flex: 1">
    <div class="main-part">
        <?php foreach ($this->messages as $message): ?>
            <div class="flash-message <?= $message->type; ?>">
                <span class="close">X</span>

                <div class="wrapper">
                    <?= $message->message; ?>
                </div>
            </div>
        <?php endforeach; ?>

        <?= $this->layout()->content; ?>
    </div>
</div>


<?php if ($this->newChild): ?>
    <div id="newChildSurvey" title="New Child Survey" style="display: none; background: #fff;">
        <h2>Child survey (optional)</h2>

        <p>We've noticed you've added <b><?= $this->newChild->get('firstName'); ?> <?= $this->newChild->get('lastName'); ?></b> to Tiny Tastes PRO Cloud Services.</p>

        <p>Would you please complete this brief survey?*</p>

        <form class="form" action="/account/new-child-survey" method="post">
            <p class="full-width">
                <label for="followed">
                    <input type="checkbox" name="followed" data-if-checked-show="#followedDetails"/>
                    <span class="checkbox-label">Child is seeing a professional to help with feeding/nutrition</span>
                </label>
            </p>

            <div id="followedDetails" style="display: none;">
                <p class="full-width">
                    <label for="speciality">
                        <span>Which professional(s)?</span>
                        <span style="float: left; width: 50%; clear: none; text-align: left;">
                            <?php foreach ([
                                               "Pediatrician",
                                               "Occupational Therapist",
                                               "Speech Language Pathologist",
                                               "Pediatric Dietitian/Nutritionist",
                                               "Psychologist/Psychiatrist",
                                               "Developmental Behavioral Specialist",
                                               "Child Life Specialist",
                                               "Family Physician",
                                               "Other"
                                           ] as $value):?>
                                <input type="checkbox" name="speciality[]" value="<?= $value; ?>"/>
                                <span class="checkbox-label"><?= $value; ?></span>
                                <br/>
                            <?php endforeach; ?>
                        </span>
                    </label>
                </p>

                <p class="full-width">
                    <label for="frequency">
                        <span>How frequently?</span>
                        <select name="frequency" id="frequency">
                            <?php foreach ([
                                               "More than 1 x/wk",
                                               "Once a week",
                                               "Bi-weekly",
                                               "Once a month",
                                               "Less than monthly",
                                               "Not regularly"
                                           ] as $value): ?>
                                <option value="<?= $value; ?>"><?= $value; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </label>
                </p>
            </div>

            <p class="full-width">
                <label for="diagnosed">
                    <input type="checkbox" name="diagnosed" data-if-checked-show="#diagnosedDetailsYes" data-if-checked-hide="#diagnosedDetailsNo"/>
                    <span class="checkbox-label">Child been diagnosed with feeding challenges by a medical professional</span>
                </label>
            </p>

            <div id="diagnosedDetailsYes" style="display: none;">
                <p class="full-width">
                    <label for="symptoms">
                        <span>Which does apply?</span>
                        <span style="float: left; width: 50%; clear: none; text-align: left;">
                            <?php foreach ([
                                               "Severe food aversions",
                                               "Texture limitations",
                                               "Sensory issues",
                                               "Autism spectrum",
                                               "Swallowing difficulties",
                                               "Chewing difficulties",
                                               "Drinking difficulties",
                                               "Excessive eating time",
                                               "Limited food colors",
                                               "Vegetable avoidance",
                                               "Lack of appetite",
                                               "Underweight"
                                           ] as $value): ?>
                                <input type="checkbox" name="symptoms[]" value="<?= $value; ?>"/>
                                <span class="checkbox-label"><?= $value; ?></span>
                                <br/>
                            <?php endforeach; ?>
                        </span>
                    </label>
                </p>
            </div>

            <div id="diagnosedDetailsNo">
                <p class="full-width">
                    <label for="symptoms2">
                        <span>Which does apply?</span>
                        <span style="float: left; width: 50%; clear: none; text-align: left;">
                            <?php foreach ([
                                               "Selective eating",
                                               "Texture issues",
                                               "Takes long to eat",
                                               "Difficulty drinking",
                                               "Low appetite",
                                               "Limits vegetables",
                                               "Limits fruits",
                                               "Limits proteins"
                                           ] as $value): ?>
                                <input type="checkbox" name="symptoms[]" value="<?= $value; ?>"/>
                                <span class="checkbox-label"><?= $value; ?></span>
                                <br/>
                            <?php endforeach; ?>
                        </span>
                    </label>
                </p>
            </div>

            <p class="full-width" style="text-align: center;">
                <input type="submit" name="skip" value="Skip"/>
                <input type="submit" name="submit" value="Submit"/>
            </p>

            <input type="hidden" name="child" value="<?= $this->newChild->getObjectId(); ?>"/>

            <?= $this->CSRF(); ?>
        </form>

        <p>*Information is for statistical purposes only and will never be shared in a personally-identifiable way; we respect your privacy.</p>
    </div>

<?php $this->placeholder('scripts')->captureStart(); ?>
    <script>
        $(document).ready(function () {

            var dialog = $("#newChildSurvey").dialog({
                modal: true,
                draggable: false,
                resizable: false,
                width: 1000
            }).dialog("open");


            $('input[type=checkbox]').on('change', function () {
                if ($(this).prop('checked')) {
                    if ($(this).data('if-checked-show')) {
                        $($(this).data('if-checked-show')).show();
                    }
                    if ($(this).data('if-checked-hide')) {
                        $($(this).data('if-checked-hide')).hide();
                    }
                } else {
                    if ($(this).data('if-checked-show')) {
                        $($(this).data('if-checked-show')).hide();
                    }
                    if ($(this).data('if-checked-hide')) {
                        $($(this).data('if-checked-hide')).show();
                    }
                }
            });
        });
    </script>
    <?php $this->placeholder('scripts')->captureEnd() ?>
<?php endif; ?>

<footer style="
padding: 0 40px;
margin-top: 50px;
">
    <div class="copyright" style="
padding: 20px 0;
text-align: center;
font-weight: bold;
background: #d6d6d6;
">
        <span>© Copyright 2017 Little Turtle</span>
    </div>
</footer>
<?= $this->placeholder('scripts') ?>
</body>
</html>