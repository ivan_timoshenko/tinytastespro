<?php $this->layout()->setLayout('public'); ?>
<?php if ($this->messages && count($this->messages)): ?>
    <div class="container" style="flex: 1">
        <div class="main-part">
            <?php foreach ($this->messages as $message): ?>
                <div class="flash-message <?= $message->type; ?>">
                    <span class="close">X</span>

                    <div class="wrapper">
                        <?= $message->message; ?>
                    </div>
                </div>
            <?php endforeach; ?>

            <?= $this->layout()->content; ?>
        </div>
    </div>
<?php endif ?>
<form class="form" action="" id="loginForm" method="post">
    <p class="full-width">
        <label for="email">
            <span>Email *</span>
            <input type="text" name="email" id="email"/>
        </label>
    </p>

    <p class="full-width">
        <label for="password">
            <span>Password</span>
            <input type="password" name="password" id="password"/>
        </label>
    </p>

    <p class="full-width" style="text-align: center;">
        <input type="submit" value="Login"/>
    </p>

    <p style="text-align: center; font-size: 1.125em; line-height: 1.25em;">
        Professionals, simply log in here if you have a clinical account, or visit <br/>
        the <a href="/for-professionals/pricing">Professionals pricing tab</a> to register for one.
    </p>

    <p style="text-align: center; font-size: 1.125em; line-height: 1.25em;">
        If you are a family/caregiver already using the Tiny Tastes app, please <br/>
        use the same login information that you have created <br/>
        for use in the app, or create a new profile below.
    </p>

    <p style="text-align: center; font-size: 1.125em; line-height: 1.25em;">
        <a href="/user/register-family">No account yet?</a>
    </p>

    <p style="text-align: center; font-size: 1.125em; line-height: 1.25em;">
        <a href="/user/forgot-password">Forgot the password?</a>
    </p>

    <?= $this->CSRF(); ?>
</form>

<script type="text/javascript">
    $(document).ready(function () {
        $('#loginForm input[type="submit"]').on('click', function () {
            localStorage.removeItem("select_tab");
        });
    });
</script>