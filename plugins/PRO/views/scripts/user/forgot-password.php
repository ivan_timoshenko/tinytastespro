<?php $this->layout()->setLayout('public'); ?>

<?php if ($this->messages && count($this->messages)): ?>
    <div class="container" style="flex: 1">
        <div class="main-part">
            <?php foreach ($this->messages as $message): ?>
                <div class="flash-message <?= $message->type; ?>">
                    <span class="close">X</span>

                    <div class="wrapper">
                        <?= $message->message; ?>
                    </div>
                </div>
            <?php endforeach; ?>

            <?= $this->layout()->content; ?>
        </div>
    </div>
<?php endif ?>
    <form action="" class="form" method="post" id="registerProfessional">
        <h2>Please enter the email you used for app registration and we will send you a new password via email</h2>

        <p class="full-width">
            <label for="email">
                <span>Email:</span>
                <input type="text" name="email" id="email" value="<?= isset($_POST['email']) ? $_POST['email'] : null; ?>"/>
            </label>
        </p>

        <p class="error-notification" style="display: none;">
            Some of the fields are not filled or filled incorrectly.
        </p>

        <p class="full-width" style="text-align: center;">
            <input type="submit" value="Submit"/>
        </p>

        <?= $this->CSRF(); ?>
    </form>

    <?php $this->placeholder('scripts')->captureStart(); ?>
    <script>
        $(document).ready(function () {
            $('form#registerProfessional').validate({
                debug: true,
                invalidHandler: function (event, validator) {
                    $(this).find('.error-notification').show();
                },
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                },
                errorPlacement: function (error, element) {
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        });
    </script>
    <?php $this->placeholder('scripts')->captureEnd() ?>
