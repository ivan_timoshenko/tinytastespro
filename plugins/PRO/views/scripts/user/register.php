<?php $this->layout()->setLayout('public'); ?>

<?php if ($this->errors && count($this->errors)): ?>
    <div class="error-screen">
        <h2>Unfortunately your account couldn't be registered</h2>
        <div class="error-description">
            <p>Here are the reasons:</p>
            <?php foreach ($this->errors as $error): ?>
                <p><?= $error; ?></p>
            <?php endforeach; ?>
            <p>Please try again later or contact us if the issue persists.</p>
        </div>
        <section class="body-section">
            <h2>Questions?</h2>
            <div class="wrapper">
                <div style="float: left; width: 49%; text-align: right;">
                    <p>
                        Feel free to email us <br/>
                        for more information:
                    </p>
                </div>

                <div style="float: left; width: 45%; text-align: left; padding: 12px 0 0 30px;">
                    <p>
                        <a class="button mail gray" href="mailto:info@tinytastesworld.com">info@</a>
                    </p>
                </div>
            </div>
        </section>
    </div>
<?php else: ?>
    <form action="" class="form" method="post" id="registerProfessional">
        <h2>Personal info</h2>

        <p class="full-width">
            <label for="firstName">
                <span>First name:</span>
                <input type="text" name="firstName" id="firstName" value="<?= isset($_POST['firstName']) ? $_POST['firstName'] : null; ?>"/>
            </label>
        </p>

        <p class="full-width">
            <label for="lastName">
                <span>Last name:</span>
                <input type="text" name="lastName" id="lastName" value="<?= isset($_POST['lastName']) ? $_POST['lastName'] : null; ?>"/>
            </label>
        </p>

        <p class="full-width">
            <label for="speciality">
                <span>Speciality</span>
                <select name="speciality" id="speciality">
                    <?php foreach ([
                                       "Pediatrician",
                                       "Occupational Therapist",
                                       "Speech Language Pathologist",
                                       "Pediatric Dietitian/Nutritionist",
                                       "Psychologist/Psychiatrist",
                                       "Developmental Behavioral Specialist",
                                       "Child Life Specialist",
                                       "Family Physician",
                                       "Other"
                                   ] as $value): ?>
                        <option value="<?= $value; ?>" <?= isset($_POST['speciality']) && $value == $_POST['speciality'] ? 'selected' : ''; ?>><?= $value; ?></option>
                    <?php endforeach; ?>
                </select>
            </label>
        </p>

        <p class="full-width">
            <label for="email">
                <span>Email:</span>
                <input type="text" name="email" id="email" value="<?= isset($_POST['email']) ? $_POST['email'] : null; ?>"/>
            </label>
        </p>

        <p class="full-width">
            <label for="password">
                <span>Password:</span>
                <input type="password" name="password" id="password"/>
            </label>
        </p>

        <p class="full-width">
            <label for="passwordRep">
                <span>Confirm password:</span>
                <input type="password" name="passwordRep" id="passwordRep"/>
            </label>
        </p>

        <p class="full-width">
            <label for="preferredLanguage">
                <span>Preferred Language:</span>
                <?= $this->entitySelect('preferredLanguage', $this->options->language, 'name', isset($_POST['preferredLanguage']) ? $_POST['preferredLanguage'] : null); ?>
            </label>
        </p>

        <h2>Clinic information</h2>

        <p class="full-width">
            <label for="clinicName">
                <span>Clinic name:</span>
                <input type="text" name="clinicName" id="clinicName" value="<?= isset($_POST['clinicName']) ? $_POST['clinicName'] : null; ?>"/>
            </label>
        </p>

        <p class="full-width">
            <label for="country">
                <span>Country:</span>
                <?= $this->entitySelect('country', $this->options->country, 'name', isset($_POST['country']) ? $_POST['country'] : null); ?>
            </label>
        </p>

        <p class="full-width">
            <label for="address">
                <span>Street Address:</span>
                <input type="text" name="address" id="address" value="<?= isset($_POST['address']) ? $_POST['address'] : null; ?>"/>
            </label>
        </p>

        <p class="full-width">
            <label for="city">
                <span>City:</span>
                <input type="text" name="city" id="city" value="<?= isset($_POST['city']) ? $_POST['city'] : null; ?>"/>
            </label>
        </p>

        <p class="full-width">
            <label for="state">
                <span>State:</span>
                <?= $this->entitySelect('state', $this->options->state, 'name', isset($_POST['state']) ? $_POST['state'] : null); ?>
            </label>
        </p>

        <p class="full-width">
            <label for="postCode">
                <span>Postal Code:</span>
                <input type="text" name="postCode" id="postCode" value="<?= isset($_POST['postCode']) ? $_POST['postCode'] : null; ?>"/>
            </label>
        </p>

        <p class="full-width">
            <label for="phoneNumber">
                <span>Clinic Phone number:</span>
                <input type="text" name="phoneNumber" id="phoneNumber" value="<?= isset($_POST['phoneNumber']) ? $_POST['phoneNumber'] : null; ?>"/>
            </label>
        </p>

        <p class="error-notification" style="display: none;">
            Some of the fields are not filled or filled incorrectly.
        </p>

        <p class="full-width" style="text-align: center;">
            <input type="submit" value="Submit"/>
        </p>

        <?= $this->CSRF(); ?>
    </form>

    <?php $this->placeholder('scripts')->captureStart(); ?>
    <script>
        $(document).ready(function () {
            $('form#registerProfessional').validate({
                debug: true,
                invalidHandler: function (event, validator) {
                    $(this).find('.error-notification').show();
                },
                rules: {
                    firstName: {
                        required: true
                    },
                    lastName: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true
                    },
                    passwordRep: {
                        equalTo: "#password"
                    },
                    country: {
                        required: true
                    },
                    state: {},
                    city: {}
                },
                errorPlacement: function (error, element) {
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        });
    </script>
    <?php $this->placeholder('scripts')->captureEnd() ?>
<?php endif; ?>
