<?php $this->layout()->setLayout('public'); ?>

<?php if ($this->errors && count($this->errors)): ?>
    <div class="error-screen">
        <h2>Unfortunately your account couldn't be registered</h2>

        <p>Here are the reasons:</p>
        <?php foreach ($this->errors as $error): ?>
            <p><?= $error; ?></p>
        <?php endforeach; ?>
        <p>Please try again later or contact us if the issue persists.</p>
    </div>
<?php endif; ?>

    <form action="" class="form" method="post" id="setPassword">
        <h2>Choose Name, Last Name and password</h2>
        <p class="full-width">
            <label for="firstName">
                <span>First name:</span>
                <input type="text" name="firstName" id="firstName" value="<?= isset($_POST['firstName']) ? $_POST['firstName'] : null; ?>"/>
            </label>
        </p>

        <p class="full-width">
            <label for="lastName">
                <span>Last name:</span>
                <input type="text" name="lastName" id="lastName" value="<?= isset($_POST['lastName']) ? $_POST['lastName'] : null; ?>"/>
            </label>
        </p>
        <p class="full-width">
            <label for="password">
                <span>Password:</span>
                <input type="password" name="password" id="password"/>
            </label>
        </p>

        <p class="full-width">
            <label for="passwordRep">
                <span>Confirm password:</span>
                <input type="password" name="passwordRep" id="passwordRep"/>
            </label>
        </p>

        <p class="error-notification" style="display: none;">
            Some of the fields are not filled or filled incorrectly.
        </p>

        <p class="full-width" style="text-align: center;">
            <input type="submit" value="Submit"/>
        </p>

        <input type="hidden" name="id" value="<?= $this->invitation->getObjectId(); ?>"/>
        <?= $this->CSRF(); ?>
    </form>

<?php $this->placeholder('scripts')->captureStart(); ?>
    <script>
        $(document).ready(function () {
            $('form#setPassword').validate({
                debug: true,
                invalidHandler: function (event, validator) {
                    $(this).find('.error-notification').show();
                },
                rules: {
                    password: {
                        required: true
                    },
                    passwordRep: {
                        equalTo: "#password"
                    }
                },
                errorPlacement: function (error, element) {
                },
                submitHandler: function (form) {
                    form.submit();
                }
            });
        });
    </script>
<?php $this->placeholder('scripts')->captureEnd() ?>