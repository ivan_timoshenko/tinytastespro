<html>
<head></head>
<body>
<p>
    Hello <?= $this->invitation->get('firstName'); ?>,
</p>
<br/>
<p>
    You have been invited to use Tiny Tastes PRO
    by <?= $this->inviter->get('initials'); ?> <?= $this->inviter->get('firstName'); ?> <?= $this->inviter->get('lastName'); ?>
    from <?= $this->invitation->get('clinic')->get('name'); ?> since you are the responsible guardian of one of their
    patients.
</p>

<p>
    This secure online service will allow you and this clinician to view what your child eats at home over time, to gain
    insights into their intake-trends and progress.
</p>

<p>
    Please confirm that you would like to utilize this service to share your child's dietary information with this
    provider by activating your free account access
    <a href="http://<?= $_SERVER['HTTP_HOST']; ?>/user/accept-family-to-clinic-invitation?id=<?= $this->invitation->getObjectId(); ?>">here</a>
</p>

<p>
    In the future, you will be able to log in to your account at <a href="http://www.tinytastespro.com">www.tinytastespro.com</a>
    where you can learn more about the Tiny Tastes PRO web service and see how it interacts with your family's use of
    the <a href="https://itunes.apple.com/us/app/tiny-tastes/id926928170">Tiny Tastes</a> mobile app.
</p>

<p>
    If you haven't downloaded the Tiny Tastes app yet, you can find it in the App Store of your mobile device (tablet
    and/or smartphone). For an in-app purchase cost below $3.00, you will then also be able to participate in the free
    Tiny Tastes PRO web service, which is being offered to you here from your healthcare professional at no cost to you.
</p>

<p>
    If you have any further questions after visiting <a href="http://www.tinytastespro.com">TinyTastesPro.com</a>,
    please feel free to reply to this email and we will be happy to respond in a timely fashion.
</p>

<p>
    Best regards,
    <br/>
    The Tiny Tastes Team
</p>
</body>
</html>