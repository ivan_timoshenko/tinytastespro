<html>
<head></head>
<body>


<p>Hello <?= $this->invitation->get('firstName'); ?>,</p>
<br/>
<p>
    You have been invited to use Tiny Tastes PRO
    by <?= $this->inviter->get('firstName'); ?> <?= $this->inviter->get('lastName'); ?>
    since you participate with them in the care of the same child(ren).
</p>

<p>
    This secure online service allows you to view what a child has been fed & what they have actually been eating, to
    help monitor their intake and progress.
</p>

<p>
    Please confirm your interest in using this service by activating your free account access
    <a href="http://<?= $_SERVER['HTTP_HOST']; ?>/user/accept-family-invitation?id=<?= $this->invitation->getObjectId(); ?>">here</a>.
    <br/>
    In the future, you will be able to log in to your account at <a href="http://www.tinytastespro.com">www.tinytastespro.com</a>
    where you can learn more about the Tiny Tastes PRO web service and see how it intersects with the Tiny Tastes mobile
    app.
</p>

<p>
    Best regards,
    <br/>
    The Tiny Tastes Team
</p>
</body>
</html>