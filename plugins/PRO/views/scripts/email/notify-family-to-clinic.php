<html>
<head></head>
<body>

<p>
    Your colleague <?= $this->inviter->get('firstName'); ?> <?= $this->inviter->get('lastName'); ?>
    has invited <?= $this->invitation->get('firstName'); ?> <?= $this->invitation->get('lastName'); ?>
    to become a patient of your clinic.
</p>

<p>
    Please make sure to keep track of vacant slots in you clinic subscription in order to make sure your
    colleagues are able to invite more families.
</p>

<p>
    Best regards,
    <br/>
    The Tiny Tastes Team
</p>
</body>
</html>