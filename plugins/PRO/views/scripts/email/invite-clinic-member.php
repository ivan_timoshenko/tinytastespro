<html>
<head></head>
<body>

<p>Hello <?= $this->invitation->get('firstName'); ?>,</p>
<br/>
<p>
    You have been invited to use Tiny Tastes PRO by your account administrator
    <?= $this->inviter->get('firstName'); ?> <?= $this->inviter->get('lastName'); ?> .
</p>

<p>
    This secure online service will allow you to view what your patients/clients have been fed & what they have actually
    eaten at home since you've seen them last.
    <br/>
    Please confirm your interest in using this service by activating your account
    <a href="http://<?= $_SERVER['HTTP_HOST']; ?>/user/accept-clinic-invitation?id=<?= $this->invitation->getObjectId(); ?>">here</a>.
    <br/>
    In the future, you will be able to log in to your account at <a href="http://www.tinytastespro.com">www.tinytastespro.com</a>
    where you can learn more about Tiny Tastes PRO and invite the families you work with to activate their free PRO
    account through your group's plan. Once you confirm your own account, you will be able to start inviting parents to
    join through your Manage Account
    tab.
</p>

<p>
    Best regards,
    <br/>
    The Tiny Tastes Team
</p>
</body>
</html>