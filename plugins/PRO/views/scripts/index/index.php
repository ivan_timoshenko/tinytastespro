<?php if ($this->currentUser): ?>
    <?php $this->layout()->setLayout('protected'); ?>

    <h1 style="text-align: center;">Welcome, <?= $this->currentUser->get('person')->get('firstName'); ?></h1>

    <p style="text-align: center;">Please use main menu to start working.</p>
<?php else: ?>
    <?php $this->layout()->setLayout('public'); ?>

    <h1 style="text-align: center;">Welcome to Tiny Tastes PRO!</h1>

    <p style="text-align: center;">If you want to read more, please proceed to our FAQ section (pending)</p>

    <p style="text-align: center;">If you already have an account, please use the form below to login</p>

    <p>&nbsp;</p>

    <form class="form" action="/user/login" method="post">
        <p class="full-width">
            <label for="email">
                <span>Email</span>
                <input type="text" name="email" id="email"/>
            </label>
        </p>

        <p class="full-width">
            <label for="password">
                <span>Password</span>
                <input type="password" name="password" id="password"/>
            </label>
        </p>

        <p class="full-width" style="text-align: center;">
            <input type="submit" value="Login"/>
        </p>

        <?= $this->CSRF(); ?>
    </form>
<?php endif; ?>