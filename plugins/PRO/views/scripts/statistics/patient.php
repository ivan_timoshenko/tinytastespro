<?php $this->layout()->setLayout('protected'); ?>

    <nav class="patient-selector">
        Patient / Child:
        <?= $this->currentPatient ? $this->currentPatient->get('firstName') . ' ' . $this->currentPatient->get('lastName') : 'Not Yet Chosen'; ?>
        <div class="switch-wrapper">
            <a class="switch modal" href="#choosePatient" class="modal"><?= $this->currentPatient ? 'Switch' : 'Choose'; ?></a>
        </div>
    </nav>

<?php if ($this->session->getRole() == 'family-manager' || $this->session->getRole() == 'family-non-manager'): ?>
    <div id="choosePatient" title="Select Child" style="display: none;">
        <?php if (!count($this->patients)): ?>
            <p style="font-size: 1.12em; line-height: 1.5em;">
                To connect your child's Tiny Tastes information to this website
                <br/>
                visit app's Tiny Shop and upgrade there to begin Cloud Sharing.
                <br/>
                Then Customize User Profiles in the app's Settings.
            </p>
        <?php else: ?>
            <ul>
                <?php foreach ($this->patients as $patient): ?>
                    <li>
                        <a href="<?= $this->url(['controller' => 'Statistics', 'action' => 'patient', 'patient' => $patient->getObjectId()], 'PRO', true); ?>"><?= $patient->get('firstName'); ?> <?= $patient->get('lastName'); ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </div>
<?php else: ?>
    <div id="choosePatient" title="Select Child" style="display: none;">
        <form class="form" action="/statistics/patient" method="post">
            <p class="full-width">
                <label for="firstName">
                    <span>First name:</span>
                    <input type="text" name="firstName" id="firstName" value="<?= isset($_POST['firstName']) ? $_POST['firstName'] : null; ?>"/>
                </label>
            </p>

            <p class="full-width">
                <label for="lastName">
                    <span>Last name:</span>
                    <input type="text" name="lastName" id="lastName" value="<?= isset($_POST['lastName']) ? $_POST['lastName'] : null; ?>"/>
                </label>
            </p>

            <p class="full-width" style="text-align: center;">
                <input type="submit" value="Find"/>
            </p>

            <?= $this->CSRF(); ?>
        </form>
    </div>
<?php endif; ?>

<?php if ($this->currentPatient): ?>
    <div class="tabbed-content patient-manager">
        <ul class="main-menu">
            <li><a href="#tab-chart">Intake chart</a></li>
            <li><a href="#tab-statistics">Statistics</a></li>
        </ul>

        <div id="tab-chart" class="meals-calendar-container">
            <p class="tip">
                Hover over any thumbnail to see larger image.
                <br/>
                When food was TASTED, hovering over thumbnails will show the images of the plate Before and After it was
                tasted.
            </p>

            <div class="meals-calendar">
            </div>
        </div>

        <div id="tab-statistics" class="statistic-page">
            <h2>Number of Tiny Tastes User Per Day</h2>
            <div id="chart-daily-meals" class="chart-container">   <canvas id="myChartEat"></canvas></div>

            <h2>Minutes spent eating</h2>
            <div id="chart-meals-duration"class="chart-container"> <canvas id="myChartEatType"></canvas></div>

            <h2>Reported Intake Frequency</h2>
            <div id="chart-meals-status"class="chart-container">  <canvas id="myChartEatCount"></canvas></div>
        </div>
    </div>

    <div id="tooltipAligner" style="position:fixed; top: 0; left: 0; width: 1px; height: 1px;">
        &nbsp
        <!-- this top left aligned fixed element serves a basis for meal tooltip-->
    </div>
    <?php $this->placeholder('scripts')->captureStart(); ?>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datejs/1.0/date.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script>
        var ctx = document.getElementById("myChartEat").getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [   <?php foreach ($this->mealsStatistics['data_chart'] as $key=>$data){?>'<?=$key?>', <?php } ?>],
                datasets: [
                    {
                        label: 'None',
                        backgroundColor: 'rgb(255, 99, 132)',
                        data: [<?= implode(',',$this->mealsStatistics['data_none'])?>]
                    }, {
                        label: 'Tasted',
                        backgroundColor: 'rgb(150, 229, 112)',
                        data: [<?= implode(',',$this->mealsStatistics['data_tasted'])?>]
                    }, {
                        label: 'All eaten',
                        backgroundColor: 'rgb(255, 199, 252)',
                        data: [<?= implode(',',$this->mealsStatistics['data_all_eaten'])?>]
                    },
                ]
            },
            options: {
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                responsive: true,
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
        });
        var ctxType = document.getElementById("myChartEatType").getContext('2d');
        var timeFormat = 'MM/DD/YYYY HH:mm';

        var myChartType = new Chart(ctxType, {
            type: 'line',
            data: {
                labels: [
                    <?php foreach ($this->mealsStatistics['data_week'] as $day) { ?>
                    '<?=$day?>',
                    <?php } ?>
                ],
                datasets: [{
                    label: 'Breakfast',
                    data: [<?=implode(',',$this->mealsStatistics['data_week_statistic']['mealtype_breakfast']) ?>],
                    fill: false,
                    backgroundColor: 'rgba(255, 99, 132, 0.5)',
                    borderColor: 'rgb(255, 99, 132)',
                },{
                    label: 'Lunch',
                    data: [<?=implode(',',$this->mealsStatistics['data_week_statistic']['mealtype_lunch']) ?>],
                    fill: false,
                    backgroundColor: 'rgba(15, 99, 132, 0.5)',
                    borderColor: 'rgb(15, 99, 132)',
                },{
                    label: 'Dinner',
                    data: [<?=implode(',',$this->mealsStatistics['data_week_statistic']['mealtype_dinner']) ?>],
                    fill: false,
                    backgroundColor: 'rgba(105, 97, 102, 0.5)',
                    borderColor: 'rgb(105, 97, 102)',
                }, {
                    label: 'Snacks',
                    data: [<?=implode(',',$this->mealsStatistics['data_week_statistic']['mealtype_snack']) ?>],
                    fill: false,
                    backgroundColor: 'rgba(94, 102, 12, 0.5)',
                    borderColor: 'rgb(94, 102, 12)',
                 },
                {
                    label: 'Drinks',
                    data: [<?=implode(',',$this->mealsStatistics['data_week_statistic']['mealtype_drink']) ?>],
                    fill: false,
                    backgroundColor: 'rgba(9, 12, 54, 0.5)',
                    borderColor: 'rgb(9, 12, 54)',
                },]
            },
            options: {
                scales: {
                    xAxes: [{
                        time: {
                            unit: 'week',
                            format: timeFormat,
                            tooltipFormat: 'll HH:mm'
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Date'
                        }
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Minutes'
                        },
                        max: 60,
                    }]
                },
            }
        });
        var ctxCount = document.getElementById("myChartEatCount").getContext('2d');
        var timeFormat = 'MM/DD/YYYY HH:mm';

        var myChartCount = new Chart(ctxCount, {
            type: 'line',
            data: {
                labels: [
                    <?php foreach ($this->mealsStatistics['data_week'] as $day) { ?>
                    '<?=$day?>',
                    <?php } ?>
                ],
                datasets: [{
                    label: 'Number of meals',
                    data: [<?=implode(',',$this->mealsStatistics['data_chart_count_data']) ?>],
                    fill: false,
                    backgroundColor: 'rgba(255, 99, 132, 0.5)',
                    borderColor: 'rgb(255, 99, 132)',
                },]
            },
            options: {
                scales: {
                    xAxes: [{
                        time: {
                            unit: 'week',
                            format: timeFormat,
                            tooltipFormat: 'll HH:mm'
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Date'
                        }
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: 'Number'
                        },
                        max: 10,
                    }]
                },
            }
        });
        var data = <?= $this->mealsSerialized; ?>;
        google.load('visualization', '1.0', {packages: ['corechart', 'bar']});


       /* function drawCharts(meals) {
            mealsByDay = breakMealsByDay(meals);

            // Meals by day
            var data = new google.visualization.DataTable();
            data.addColumn('date', 'day');
            data.addColumn('number', '');

            for (var i = 0; i < mealsByDay.length; i++) {
                data.addRow([new Date(mealsByDay[i][0].started.date), mealsByDay[i].length]);
            }

            var chart = new google.visualization.LineChart(document.getElementById('chart-daily-meals'));
            chart.draw(data, {
                width: 1000,
                height: 400,
                hAxis: {
                },
                vAxis: {
                    title: '',
                    minValue: 0,
                    format: '0'
                }
            });


            // Meals duration
            var data = new google.visualization.DataTable();
            data.addColumn('date', 'day');
            data.addColumn('number', 'Breakfast');
            data.addColumn('number', 'Lunch');
            data.addColumn('number', 'Dinner');
            data.addColumn('number', 'Snacks');
            data.addColumn('number', 'Drinks');

            for (var i = 0; i < mealsByDay.length; i++) {
                row = {
                    mealtype_breakfast: 0,
                    mealtype_lunch: 0,
                    mealtype_dinner: 0,
                    mealtype_morning_snack: 0,
                    mealtype_afternoon_snack: 0,
                    mealtype_evening_snack: 0,
                    mealtype_drink: 0
                };
                for (var j = 0; j < mealsByDay[i].length; j++) {
                    row[mealsByDay[i][j].mealType] += (mealsByDay[i][j].finished && mealsByDay[i][j].started) ? (Date.parse(mealsByDay[i][j].finished.date) - Date.parse(mealsByDay[i][j].started.date)) / 1000 / 60 : 0;
                }

                data.addRow([
                    new Date(mealsByDay[i][0].started.date),
                    row.mealtype_breakfast,
                    row.mealtype_lunch,
                    row.mealtype_dinner,
                    row.mealtype_morning_snack + row.mealtype_afternoon_snack + row.mealtype_evening_snack,
                    row.mealtype_drink
                ]);
            }

            var chart = new google.visualization.LineChart(document.getElementById('chart-meals-duration'));
            chart.draw(data, {
                width: 1000,
                height: 400,
                hAxis: {jr
                },
                vAxis: {
                    title: '',
                    minValue: 0,
                    format: '0'
                }
            });


            // Meals finish status
            var data = new google.visualization.DataTable();
            data.addColumn('date', 'day');
            data.addColumn('number', 'None');
            data.addColumn('number', 'Tasted');
            data.addColumn('number', 'All Eaten');

            for (var i = 0; i < mealsByDay.length; i++) {
                row = [0,0,0];
                for (var j = 0; j < mealsByDay[i].length; j++) {
                    row[mealsByDay[i][j].finishOption] ++;
                }

                data.addRow([
                    new Date(mealsByDay[i][0].started.date),
                    row[0] / (row[0] + row[1] + row[2]) * 100,
                    row[1] / (row[0] + row[1] + row[2]) * 100,
                    row[2] / (row[0] + row[1] + row[2]) * 100
                ]);
            }

            var chart = new google.visualization.ColumnChart(document.getElementById('chart-meals-status'));
            chart.draw(data, {
                width: 1000,
                height: 400,
                isStacked: true,
                hAxis: {
                },
                vAxis: {
                    title: '',
                    minValue: 0,
                    isStacked: true,
                    format: '0'
                }
            });
        }*/


        function buildCalendar(meals) {
            mealsByDay = breakMealsByDay(meals);
            for (var i = 0; i < mealsByDay.length; i++) {
                buildDay(mealsByDay[i]);
            }

            function buildDay(meals) {
                if (!meals.length) return;

                var mealsContainer = $('<td class="content">');
                for (var i = meals.length - 1; i >= 0; i--) {
                    switch (meals[i].finishOption) {
                        case 0:
                            status = 'None';
                            break;
                        case 1:
                            status = 'Finished';
                            break;
                        case 2:
                            status = 'Tried';
                            break;
                        case 3:
                            status = 'Didn\'t eat'
                    }

                    meal = $('<div>').addClass("meal")
                        .append($('<header>').html(meals[i].mealTypeLocalized + ' ' + (new Date(meals[i].started)).toString('HH:mm')))
                        .append($('<div>').addClass("thumbnail-container").addClass(meals[i].mealType)
                            .append($('<img>')
                                .attr('src', meals[i].thumbnail)
                                .addClass('thumbnail')
                                .addClass('tooltip')
                                .addClass(meals[i].mealType)
                                .attr('data-tooltip', '#tooltip_' + meals[i].id)
                        )
                    )
                        .append($('<span>').addClass('status').addClass(meals[i].finishOption !== 1 ? 'warning' : '').text(status))
                        .append($('<br>'))
                        .append($('<span>').text(meals[i].notes ? meals[i].notes : ''))
                    ;

                    tooltipContent = $('<div>').css({display: 'none'})
                        .append($('<div>').attr('id', 'tooltip_' + meals[i].id).addClass('meal-tooltip').addClass(meals[i].finishOption == 2 ? 'double' : '')
                            .append($('<img>').attr('src', meals[i].photoBefore)));

                    if (meals[i].finishOption == 2) {
                        tooltipContent.find('div')
                            .append($('<img>').attr('src', meals[i].photoAfter).css({'margin-left': '5px'}));
                    }

                    meal.append(tooltipContent);

                    meal.appendTo(mealsContainer);
                }
                var dayContainer = $('<div class="day">')
                    .append($('<table width="100%">')
                        .append($('<tr>')
                            .append($('<td class="header">')
                                .append($('<span>')
                                    .append((new Date(meals[0].started)).toString('dddd'))
                                    .append($('<br>'))
                                    .append($('<strong>').text((new Date(meals[0].started)).toString('M/d/yyyy')))
                            )
                        )
                            .append(mealsContainer)
                    )
                );

                $('.meals-calendar').append(dayContainer);
            }
        }

        function breakMealsByDay(meals) {
            var dayMeals = [],
                processedDay = new Date(),
                data = [];

            processedDay.setHours(0);
            processedDay.setMinutes(0);
            processedDay.setSeconds(0);

            for (var i = 0; i < meals.length; i++) {
                mealDay = new Date(meals[i].started);
                mealDay.setHours(0);
                mealDay.setMinutes(0);
                mealDay.setSeconds(0);

                if (mealDay.toDateString() != processedDay.toDateString()) {
                    data.push(dayMeals);
                    dayMeals = [];
                    processedDay = mealDay;
                }

                dayMeals.push(meals[i]);
            }
            data.push(dayMeals);

            // Remove first null
            data.shift();

            return data;
        }

        $(document).ready(function () {
            buildCalendar(data);

            $('.meals-calendar .day').each(function (index) {
                $(this).addClass(index % 2 == 0 ? 'odd' : 'even');
            });

            $(document).tooltip({
                content: function () {
                    return $($(this).data('tooltip')).clone();
                },
//                track: true,
                position: {
                    my: 'left bottom',
                    at: 'left bottom',
                    of: $('#tooltipAligner')
                },
                show: false,
                hide: false,
                items: ".tooltip"
            });


            // draw chart
          // drawCharts(data);
        });
    </script>
    <?php $this->placeholder('scripts')->captureEnd() ?>
<?php endif; ?>