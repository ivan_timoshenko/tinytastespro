<?php if ($this->currentUser): ?>
    <?php $this->layout()->setLayout('protected'); ?>
<?php else: ?>
    <?php $this->layout()->setLayout('public'); ?>
<?php endif; ?>

<?= $this->wysiwyg('content', ['width' => 960, 'height' => 600]); ?>