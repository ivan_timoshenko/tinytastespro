<h2>Invite Others to View Intake</h2>
<form class="form" action="/invitation/invite-family-member" method="post" id="inviteFamilyMember">
    <p class="full-width">
        <label for="firstName">
            <span>First name:</span>
            <input type="text" name="firstName" id="firstName" value=""/>
        </label>
    </p>

    <p class="full-width">
        <label for="lastName">
            <span>Last name:</span>
            <input type="text" name="lastName" id="lastName" value=""/>
        </label>
    </p>

    <p class="full-width">
        <label for="email">
            <span>Email:</span>
            <input type="text" name="email" id="email" value=""/>
        </label>
    </p>

    <p class="full-width" style="text-align: center;">
        <input type="submit" value="Submit"/>
    </p>

    <?= $this->CSRF(); ?>
</form>

<?php $this->placeholder('scripts')->captureStart(); ?>
<script>
    $(document).ready(function () {
        $('form#inviteFamilyMember').validate({
            debug: true,
            invalidHandler: function (event, validator) {
                $(this).find('.error-notification').show();
            },
            rules: {
                email: {
                    required: true,
                    email: true
                },
                firstName: {
                    required: false
                },
                lastName: {
                    required: false
                }
            },
            errorPlacement: function (error, element) {
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    });
</script>
<?php $this->placeholder('scripts')->captureEnd() ?>


<?php if (count($this->invitations)): ?>
    <h2>&nbsp;</h2>
    <div class="invitations person-list">
        <header>
            <table>
                <tr>
                    <td width="65%">Collaborators:</td>
                    <td width="35%">Status:</td>
                </tr>
            </table>
        </header>
        <?php foreach ($this->invitations as $invite): ?>
            <?php if ($invite->get('registeredUser') && $invite->get('registeredUser')->getObjectId() == $this->session->getFamily()->get('admin')->getObjectId()) continue; ?>
            <table>
                <tr>
                    <td width="65%"><?= $invite->get('firstName'); ?> <?= $invite->get('lastName'); ?> (<?= $invite->get('email'); ?>)</td>
                    <td width="15%"><?= $invite->get('accepted') ? 'Accepted' : 'Pending'; ?></td>
                    <td width="20%">
                        <a href="/invitation/cancel-family-member?id=<?= $invite->getObjectId(); ?>">Cancel</a>
                        <?php if (!$invite->get('accepted')): ?>
                            | <a href="/invitation/resend-family-member?id=<?= $invite->getObjectId(); ?>">Resend</a>
                        <?php endif; ?>
                    </td>
                </tr>
            </table>
        <?php endforeach; ?>
    </div>
<?php endif; ?>