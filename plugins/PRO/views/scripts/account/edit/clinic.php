<!--Profile data form-->
<form action="/account/edit-clinic" class="form" method="post" id="editClinic">
    <p class="full-width">
        <label for="name">
            <span>Clinic Name:</span>
            <input type="text" name="name" id="name" value="<?= $this->session->getClinic()->get('name'); ?>"/>
        </label>
    </p>

    <p class="full-width" style="display: none;">
        <label for="country">
            <span>Country:</span>
            <?= $this->entitySelect('country', $this->options->country, 'name',
                ($this->session->getClinic()->get('address') && $this->session->getClinic()->get('address')->get('country'))
                    ? $this->session->getClinic()->get('address')->get('country')->getObjectId() : null); ?>
        </label>
    </p>

    <p class="half-width left">
        <label for="state">
            <span>State:</span>
            <?= $this->entitySelect('state', $this->options->state, 'name',
                ($this->session->getClinic()->get('address') && $this->session->getClinic()->get('address')->get('state'))
                    ? $this->session->getClinic()->get('address')->get('state')->getObjectId() : null); ?>
        </label>
    </p>

    <p class="half-width right">
        <label for="postCode">
            <span>Zip:</span>
            <input type="text" name="postCode" id="postCode" value="<?= $this->session->getClinic()->get('address') ? $this->session->getClinic()->get('address')->get('postCode') : null; ?>"/>
        </label>
    </p>

    <p class="full-width">
        <label for="city">
            <span>City:</span>
            <input type="text" name="city" id="city" value="<?= $this->session->getClinic()->get('address') ? $this->session->getClinic()->get('address')->get('city') : null; ?>"/>
        </label>
    </p>

    <p class="full-width">
        <label for="address">
            <span>Address:</span>
            <input type="text" name="address" id="address" value="<?= $this->session->getClinic()->get('address') ? $this->session->getClinic()->get('address')->get('address') : null; ?>"/>
        </label>
    </p>

    <p class="error-notification" style="display: none;">
        Some of the fields are not filled or filled incorrectly.
    </p>

    <?= $this->CSRF(); ?>

    <p class="full-width" style="text-align: center;">
        <input type="submit" value="Submit"/>
    </p>
</form>

<?php $this->placeholder('scripts')->captureStart(); ?>
<script>
    $(document).ready(function () {
        $('form#editProfessionalProfile').validate({
            debug: true,
            invalidHandler: function (event, validator) {
                $(this).find('.error-notification').show();
            },
            rules: {
                country: {
                    required: true
                },
                state: {},
                city: {}
            },
            errorPlacement: function (error, element) {
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    })
    ;
</script>
<?php $this->placeholder('scripts')->captureEnd() ?>


<h2>Invite a new clinical user</h2>
<form class="form" method="post" action="/invitation/invite-clinic-member" id="inviteClinicMember">
    <p class="full-width">
        <label for="firstName">
            <span>First name:</span>
            <input type="text" name="firstName" id="firstName" value=""/>
        </label>
    </p>

    <p class="full-width">
        <label for="lastName">
            <span>Last name:</span>
            <input type="text" name="lastName" id="lastName" value=""/>
        </label>
    </p>

    <p class="full-width">
        <label for="email">
            <span>Email:</span>
            <input type="text" name="email" id="email" value=""/>
        </label>
    </p>

    <?= $this->CSRF(); ?>

    <p class="full-width" style="text-align: center;">
        <input type="submit" value="Submit"/>
    </p>
</form>

<?php $this->placeholder('scripts')->captureStart(); ?>
<script>
    $(document).ready(function () {
        $('form#inviteClinicMember').validate({
            debug: true,
            invalidHandler: function (event, validator) {
                $(this).find('.error-notification').show();
            },
            rules: {
                email: {
                    required: true,
                    email: true
                },
                firstName: {
                    required: true
                },
                lastName: {
                    required: true
                }
            },
            errorPlacement: function (error, element) {
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    });
</script>
<?php $this->placeholder('scripts')->captureEnd() ?>


<?php if (count($this->employees)): ?>
    <h2>&nbsp;</h2>
    <div class="invitations person-list">
        <header>
            <table>
                <tr>
                    <td width="65%">Clinical users:</td>
                    <td width="35%">Status:</td>
                </tr>
            </table>
        </header>
        <?php foreach ($this->employees as $invite): ?>
            <table>
                <tr>
                    <td width="65%"><?= $invite->get('firstName'); ?> <?= $invite->get('lastName'); ?>
                        (<?= $invite->get('email'); ?>)
                    </td>
                    <td width="15%"><?= $invite->get('accepted') ? 'Accepted' : 'Pending'; ?></td>
                    <td width="20%">
                        <a href="/invitation/cancel-clinic-member?id=<?= $invite->getObjectId(); ?>">Cancel</a>
                        <?php if (!$invite->get('accepted')): ?>
                            | <a href="/invitation/resend-clinic-member?id=<?= $invite->getObjectId(); ?>">Resend</a>
                        <?php endif; ?>
                    </td>
                </tr>
            </table>
        <?php endforeach; ?>
    </div>
<?php endif; ?>