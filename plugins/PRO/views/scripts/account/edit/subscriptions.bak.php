<div class="form subscription">
    <!-- this div.form is used just to add fake css styles to make whole page look not too bad. Real style were not defined-->

    <?php if ($this->session->getRole() == 'clinic-manager'): ?>
        <!-- Clinic -->
        <h3>CURRENTLY SUBSCRIBED FOR ACCESS TO:</h3>
        <p class="full-width">
            <label for="">
                <span>&nbsp;</span>
                <input type="text" style="width: 20%;" readonly value="Up to <?= $this->session->getSubscription()->get('type')->get('maxPatients'); ?> patient(s)"/>
                &nbsp;
                &nbsp;
                &nbsp;
                <input type="text" style="width: 20%; margin-left: 5%;" readonly value="$<?= $this->session->getSubscription()->get('type')->get('feePerMonth'); ?>/ month"/>
            </label>
        </p>
        <br/>
        <h3>NUMBER OF PATIENT ACCOUNTS IN USE:</h3>
        <p class="full-width">
            <label for="">
                <span>&nbsp;</span>
                <input type="text" style="width: 20%;" readonly value="<?= $this->patientsNum; ?>"/>
            </label>
        </p>
        <?php if (!$this->session->getSubscription()->get('valid') /*todo add validation for endDate*/): ?>
            <h2>Your subscription is not paid. You can pay here:</h2>
            <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
                <input type="hidden" name="cmd" value="_s-xclick">
                <input type="hidden" name="hosted_button_id" value="<?= $this->session->getSubscription()->get('type')->get('paypalId'); ?>">
                <input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                <img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
                <input type="hidden" name="custom" value="<?= $this->session->getSubscription()->getObjectId(); ?>">
            </form>
        <?php endif; ?>

        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>

        <h2>Change subscription plan:</h2>
        <div class="subscription-types">
            <ul>
                <?php foreach ($this->subscriptions as $subscription): ?>
                    <?php $disabled = PRO_InvitationRepository::countBy(['clinic' => $this->session->getClinic(), 'caretaker' => true]) > $subscription->get('maxPatients'); ?>
                    <li>
                        <a href="/account/change-subscription?id=<?= $subscription->getObjectId(); ?>"
                           class="item <?= $disabled ? 'disabled tooltip': ''; ?>"
                           data-tooltip="<?= $disabled ? '<span>You cannot pick this plan because number of active and pending under you clinic account exceeds maximum allowed by this plan</span>': ''?>"
                            >
                            <span class="patients">
                                <?= $subscription->get('maxPatients'); ?>
                                <br/>
                                Patient<?= $subscription->get('maxPatients') > 1 ? 's' : ''; ?>
                            </span>
                            <span class="price">
                                $<?= $subscription->get('feePerMonth'); ?>/
                                <br/>
                                month
                            </span>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>

        <?php $this->placeholder('scripts')->captureStart(); ?>
        <script>
            $(document).ready(function () {
                $('.subscription-types .item').on('click', function () {
                    return confirm(
                        'Your current subscription will be stopped without refund for current month. ' +
                        'You will have to pay for new subscription in order to continue using the system. ' +
                        'Are you sure?'
                    );
                });

                $('.subscription-types .item.disabled').off('click');
                $('.subscription-types .item.disabled').on('click', function(event){
                    event.stopImmediatePropagation();
                    return false;
                });

                $(document).tooltip({
                    content: function () {
                        return $(this).data('tooltip');
                    },
                    items: ".tooltip"
                });
            });
        </script>
        <style>
            .ui-tooltip span {
                display: inline-block;
                padding: 15px;
                text-align: center;
            }
        </style>
        <?php $this->placeholder('scripts')->captureEnd() ?>
    <?php elseif ($this->session->getRole() == 'family-manager' && !$this->session->getClinic()): ?>
        <!-- Family -->
        <p class="full-width">
            <label for="">
                <span>CURRENTLY CHARGED AMOUNT:</span>
                <input type="text" style="width: 20%; margin-left: 5%;" readonly value="$<?= $this->session->getSubscription()->get('type')->get('feePerMonth'); ?> / month"/>
            </label>
        </p>
        <?php if (!$this->session->getSubscription()->get('valid') /*todo add validation for endDate*/): ?>
            <h2>Your subscription is not paid. You can pay here:</h2>
            <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
                <input type="hidden" name="cmd" value="_s-xclick">
                <input type="hidden" name="hosted_button_id" value="<?= $this->session->getSubscription()->get('type')->get('paypalId'); ?>">
                <input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                <img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
                <input type="hidden" name="custom" value="<?= $this->session->getSubscription()->getObjectId(); ?>">
            </form>
        <?php endif; ?>
    <?php elseif ($this->session->getRole() == 'family-manager' && $this->session->getClinic()): ?>
        <!-- Family invited by clinic -->
        <h2>
            Your subscription is being paid for <br>
            by your child's medical provider, so there is <br/>
            nothing to take care of here!.
        </h2>
    <?php endif; ?>
</div>