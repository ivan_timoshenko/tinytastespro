<h2>Invite family</h2>
<form class="form" action="/invitation/invite-family-to-clinic" method="post" id="inviteFamilyToClinic">
    <p class="full-width">
        <label for="firstName">
            <span>First name:</span>
            <input type="text" name="firstName" id="firstName" value=""/>
        </label>
    </p>

    <p class="full-width">
        <label for="lastName">
            <span>Last name:</span>
            <input type="text" name="lastName" id="lastName" value=""/>
        </label>
    </p>

    <p class="full-width">
        <label for="email">
            <span>Email:</span>
            <input type="text" name="email" id="email" value=""/>
        </label>
    </p>

    <p class="full-width" style="text-align: center;">
        <input type="submit" value="Submit"/>
    </p>

    <?= $this->CSRF(); ?>
</form>

<?php $this->placeholder('scripts')->captureStart(); ?>
<script>
    $(document).ready(function () {
        $('form#inviteFamilyToClinic').validate({
            debug: true,
            invalidHandler: function (event, validator) {
                $(this).find('.error-notification').show();
            },
            rules: {
                email: {
                    required: true,
                    email: true
                },
                firstName: {
                    required: true
                },
                lastName: {
                    required: true
                }
            },
            errorPlacement: function (error, element) {
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    });
</script>
<?php $this->placeholder('scripts')->captureEnd() ?>

<?php if (count($this->invitations)): ?>
    <h2>&nbsp;</h2>
    <div class="invitations person-list">
        <header>
            <table>
                <tr>
                    <td width="65%">Invited users:</td>
                    <td width="35%">Status:</td>
                </tr>
            </table>
        </header>
        <?php foreach ($this->invitations as $invite): ?>
            <table>
                <tr>
                    <td width="65%"><?= $invite->get('firstName'); ?> <?= $invite->get('lastName'); ?> (<?= $invite->get('email'); ?>)</td>
                    <td width="15%"><?= $invite->get('accepted') ? 'Accepted' : 'Pending'; ?></td>
                    <td width="20%">
                        <a href="/invitation/cancel-family-to-clinic?id=<?= $invite->getObjectId(); ?>">Cancel</a>
                        <?php if (!$invite->get('accepted')): ?>
                            | <a href="/invitation/resend-family-to-clinic?id=<?= $invite->getObjectId(); ?>">Resend</a>
                        <?php endif; ?>
                    </td>
                </tr>
            </table>
        <?php endforeach; ?>
    </div>
<?php endif; ?>