<!--Profile data form-->
<form action="/account/edit-profile" class="form" method="post" id="editCommon" enctype="multipart/form-data">
    <!--    <p class="full-width">-->
    <!--        <label for="profileImage">-->
    <!--            <span><img src="-->
    <?php //$this->session->getPerson()->get('profileImage') ? $this->session->getPerson()->get('profileImage')->getUrl() : '/plugins/PRO/static/img/missing-photo.png'; ?><!--" alt="profile image" width="100%"/></span>-->
    <!--            <br/><br/><br/>-->
    <!--            <span>Change profile image</span>-->
    <!--            <br/><br/><br/>-->
    <!--            <input type="file" name="profileImage" id="profileImage"/>-->
    <!--        </label>-->
    <!--    </p>-->


    <p class="full-width">
        <label for="firstName">
            <span>First name:</span>
            <input type="text" name="firstName" id="firstName" value="<?= $this->session->getPerson()->get('firstName'); ?>"/>
        </label>
    </p>

    <p class="full-width">
        <label for="lastName">
            <span>Last name:</span>
            <input type="text" name="lastName" id="lastName" value="<?= $this->session->getPerson()->get('lastName'); ?>"/>
        </label>
    </p>

    <?php if ($this->session->getRole() == 'clinic-manager' || $this->session->getRole() == 'clinic-non-manager'): ?>
        <!--        <p class="full-width">-->
        <!--            <label for="initials">-->
        <!--                <span>Credentials:</span>-->
        <!--                <input type="text" name="initials" id="initials" value="--><?php //= $this->session->getPerson()->get('initials'); ?><!--"/>-->
        <!--            </label>-->
        <!--        </p>-->

        <p class="full-width">
            <label for="speciality">
                <span>Speciality</span>
                <select name="speciality" id="speciality">
                    <?php foreach ([
                                       "Pediatrician",
                                       "Occupational Therapist",
                                       "Speech Language Pathologist",
                                       "Pediatric Dietitian/Nutritionist",
                                       "Psychologist/Psychiatrist",
                                       "Developmental Behavioral Specialist",
                                       "Child Life Specialist",
                                       "Family Physician",
                                       "Other"
                                   ] as $value): ?>
                        <option value="<?= $value; ?>" <?= $value == $this->session->getProfessional()->get('speciality') ? 'selected' : ''; ?>><?= $value; ?></option>
                    <?php endforeach; ?>
                </select>
            </label>
        </p>
    <?php endif; ?>

    <p class="full-width">
        <label for="email">
            <span>Email:</span>
            <input type="text" name="email" id="email" readonly value="<?= $this->session->getUser()->email; ?>"/>
        </label>
    </p>

    <p class="full-width">
        <label for="preferredLanguage">
            <span>Preferred Language:</span>
            <?= $this->entitySelect('preferredLanguage', $this->options->language, 'name', $this->session->getPerson()->get('preferredLanguage') ? $this->session->getPerson()->get('preferredLanguage')->getObjectId() : null); ?>
        </label>
    </p>


    <?php if ($this->session->getRole() == 'family-manager'): ?>
        <p class="full-width">
            <label for="country">
                <span>Country:</span>
                <?= $this->entitySelect('country', $this->options->country, 'name',
                    ($this->session->getPerson()->get('address') && $this->session->getPerson()->get('address')->get('country'))
                        ? $this->session->getPerson()->get('address')->get('country')->getObjectId() : null); ?>
            </label>
        </p>

        <p class="half-width left">
            <label for="state">
                <span>State:</span>
                <?= $this->entitySelect('state', $this->options->state, 'name',
                    ($this->session->getPerson()->get('address') && $this->session->getPerson()->get('address')->get('state'))
                        ? $this->session->getPerson()->get('address')->get('state')->getObjectId() : null); ?>
            </label>
        </p>

        <p class="half-width right">
            <label for="city">
                <span>City:</span>
                <input type="text" name="city" id="city" value="<?= $this->session->getPerson()->get('address') ? $this->session->getPerson()->get('address')->get('city') : null; ?>"/>
            </label>
        </p>
    <?php endif; ?>

    <p class="error-notification" style="display: none;">
        Some of the fields are not filled or filled incorrectly.
    </p>

    <p class="full-width" style="text-align: center;">
        <input type="submit" value="Submit"/>
    </p>

    <?= $this->CSRF(); ?>
</form>

<?php $this->placeholder('scripts')->captureStart(); ?>
<script>
    $(document).ready(function () {
        $('form#editCommon').validate({
            debug: true,
            invalidHandler: function (event, validator) {
                $(this).find('.error-notification').show();
            },
            rules: {
                firstName: {
                    required: true
                },
                lastName: {
                    required: true
                }
            },
            errorPlacement: function (error, element) {
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    });
</script>
<?php $this->placeholder('scripts')->captureEnd() ?>



<!--Password change form-->
<h2>Change password</h2>
<form action="/account/change-password" class="form" method="post" id="changePassword">
    <p class="full-width">
        <label for="passwordOld">
            <span>Old password:</span>
            <input type="password" name="passwordOld" id="passwordOld"/>
        </label>
    </p>

    <p class="full-width">
        <label for="password">
            <span>New password:</span>
            <input type="password" name="password" id="password"/>
        </label>
    </p>

    <p class="full-width">
        <label for="passwordRep">
            <span>Confirm password:</span>
            <input type="password" name="passwordRep" id="passwordRep"/>
        </label>
    </p>

    <p class="error-notification" style="display: none;">
        Some of the fields are not filled or filled incorrectly.
    </p>

    <p class="full-width" style="text-align: center;">
        <input type="submit" value="Submit"/>
    </p>

    <?= $this->CSRF(); ?>
</form>

<?php $this->placeholder('scripts')->captureStart(); ?>
<script>
    $(document).ready(function () {
        $('form#changePassword').validate({
            debug: true,
            invalidHandler: function (event, validator) {
                $(this).find('.error-notification').show();
            },
            rules: {
                password: {
                    required: true
                },
                passwordRep: {
                    equalTo: "#password"
                }
            },
            errorPlacement: function (error, element) {
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
    });
</script>
<?php $this->placeholder('scripts')->captureEnd() ?>