<?php $this->layout()->setLayout('protected'); ?>

<?php if ($this->session->getRole() == 'clinic-manager'): ?>
    <!--    Clinic manager-->
    <div class="tabbed-content">
        <ul class="main-menu">
            <li><a href="#tab-info">Your info</a></li>
            <li><a href="#tab-professional-patients">Patients</a></li>
            <li><a href="#tab-clinic">Clinic</a></li>
            <li><a href="#tab-subscriptions">Subscriptions</a></li>
        </ul>

        <div id="tab-info"><?php $this->template('account/edit/profile.php'); ?></div>
        <div id="tab-professional-patients"><?php $this->template('account/edit/professional-patients.php'); ?></div>
        <div id="tab-clinic"><?php $this->template('account/edit/clinic.php'); ?></div>
        <div id="tab-subscriptions"><?php $this->template('account/edit/subscriptions.php'); ?></div>
    </div>
<?php elseif ($this->session->getRole() == 'clinic-non-manager'): ?>
    <!--    Clinic employee-->
    <div class="tabbed-content">
        <ul class="main-menu">
            <li><a href="#tab-info">Your info</a></li>
            <li><a href="#tab-professional-patients">Patients</a></li>
        </ul>

        <div id="tab-info"><?php $this->template('account/edit/profile.php'); ?></div>
        <div id="tab-professional-patients"><?php $this->template('account/edit/professional-patients.php'); ?></div>
    </div>
<?php elseif ($this->session->getRole() == 'family-manager'): ?>
    <!--    Family admin-->
    <div class="tabbed-content">
        <ul class="main-menu">
            <li><a href="#tab-info">Your info</a></li>
            <li><a href="#tab-invites">People invited</a></li>
            <li><a href="#tab-subscriptions">Subscriptions</a></li>
        </ul>

        <div id="tab-info"><?php $this->template('account/edit/profile.php'); ?></div>
        <div id="tab-invites"><?php $this->template('account/edit/private-invites.php'); ?></div>
        <div id="tab-subscriptions"><?php $this->template('account/edit/subscriptions.php'); ?></div>
    </div>
<?php else: ?>
    <!--    Generic family member-->
    <div class="tabbed-content">
        <ul class="main-menu">
            <li><a href="#tab-info">Your info</a></li>
        </ul>

        <div id="tab-info"><?php $this->template('account/edit/profile.php'); ?></div>
    </div>
<?php endif; ?>


<?php $this->placeholder('scripts')->captureStart(); ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('body').scrollTop(0);
            var select_tab = localStorage.getItem('select_tab');
            if( select_tab != undefined &&  select_tab.length){
                $('.tabbed-content').tabs({ active: select_tab.split('ui-id-')[1] - 1});
            }
            if($('.tabbed-content ul.main-menu').length){
                $('.tabbed-content ul.main-menu li').on('click', function(){
                    $('.container .main-part > .flash-message.success').remove();
                    localStorage.setItem('select_tab', $(this).attr('aria-labelledby'));
                });
            }
        });
    </script>
<?php $this->placeholder('scripts')->captureEnd() ?>